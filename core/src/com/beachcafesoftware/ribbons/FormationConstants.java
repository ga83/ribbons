package com.beachcafesoftware.ribbons;

import java.util.Random;

/**
 * Created by Guy Aaltonen on 8/11/17.
 */

public class FormationConstants {

    // TODO: fix the fireworks bug (looks very different depending on renderer, has bugs in some of them), and put it back in here
     public static Class[] HIGH_PROBABILITY_LIST     = new Class[] { ElectronShellsNonCoPlanar.class, LineFlightFormation.class, CircularFlightFormation.class, ChevronFlightFormation.class, RandomPositionsWithinRadiusSharedAxis.class, OrbitTheLeaderFormation.class, SpinningStickFormation.class, SwordFormation.class };
    public static Class[] MEDIUM_PROBABILITY_LIST   = new Class[] { ElectronShellsNonCoPlanar.class, LineFlightFormation.class, CircularFlightFormation.class, ChevronFlightFormation.class, RandomPositionsWithinRadiusSharedAxis.class, OrbitTheLeaderFormation.class, SpinningStickFormation.class, SwordFormation.class };
    public static Class[] LOW_PROBABILITY_LIST      = new Class[] { ElectronShellsNonCoPlanar.class, LineFlightFormation.class, CircularFlightFormation.class, ChevronFlightFormation.class, RandomPositionsWithinRadiusSharedAxis.class, OrbitTheLeaderFormation.class, SpinningStickFormation.class, SwordFormation.class };

/*
     public static Class[] HIGH_PROBABILITY_LIST     = new Class[] { CircularFlightFormation.class, ChevronFlightFormation.class, LineFlightFormation.class };
    public static Class[] MEDIUM_PROBABILITY_LIST   = new Class[] { CircularFlightFormation.class, ChevronFlightFormation.class, LineFlightFormation.class };
    public static Class[] LOW_PROBABILITY_LIST      = new Class[] { CircularFlightFormation.class, ChevronFlightFormation.class, LineFlightFormation.class };
*/

/*
      public static Class[] HIGH_PROBABILITY_LIST     = new Class[] { ChevronFlightFormation.class };
    public static Class[] MEDIUM_PROBABILITY_LIST   = new Class[] { ChevronFlightFormation.class };
    public static Class[] LOW_PROBABILITY_LIST      = new Class[] { ChevronFlightFormation.class };

*/

    /*
    public static Class[] HIGH_PROBABILITY_LIST     = new Class[] { LineFlightFormation.class };
    public static Class[] MEDIUM_PROBABILITY_LIST   = new Class[] { LineFlightFormation.class };
    public static Class[] LOW_PROBABILITY_LIST      = new Class[] { LineFlightFormation.class };
    */

    public static float MEDIUM_PROBABILITY  = 0.99f;
    public static float LOW_PROBABILITY     = 0.99f;
    public static float CHANGE_FORMATION    = 0.99f;

    static Random rnd = new Random();


    public static Class getRandomFormation(float diceRoll) {
        if(diceRoll < LOW_PROBABILITY) {
            return LOW_PROBABILITY_LIST[ rnd.nextInt(LOW_PROBABILITY_LIST.length) ];
        }
        else if(diceRoll < MEDIUM_PROBABILITY) {
            return MEDIUM_PROBABILITY_LIST[ rnd.nextInt(MEDIUM_PROBABILITY_LIST.length) ];
        }
        else {
            return HIGH_PROBABILITY_LIST[ rnd.nextInt(HIGH_PROBABILITY_LIST.length) ];
        }
    }
}
