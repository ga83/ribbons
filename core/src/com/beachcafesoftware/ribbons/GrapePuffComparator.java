package com.beachcafesoftware.ribbons;

import java.util.Comparator;

/**
 * Created by Guy Aaltonen on 27/01/19.
 */

public class GrapePuffComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {


        if(o1 == null || o2 == null) {
            try {
                throw new Exception("oops");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        SpriteLifeColor p1 = (SpriteLifeColor)o1;
        SpriteLifeColor p2 = (SpriteLifeColor)o2;

//        System.out.println("cmp " + p1.position.z + ", " + p2.position.z);

        if(p1.position.z > p2.position.z) {
            return -1;
        }
        else if(p1.position.z < p2.position.z) {
            return 1;
        }
        else return 0;
    }

}
