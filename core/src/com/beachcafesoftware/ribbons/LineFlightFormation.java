package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by ubuntu on 16/02/19.
 */

public class LineFlightFormation extends FlightFormation {

    public LineFlightFormation() {
        MINIMUM_FORMATIONS = 3;
        MAXIMUM_FORMATIONS = 3;
    }

    @Override
    protected void initFormation() {
        int numberOfRibbons = this.ribbons.size();
        float formationWidth = 300.0f;
        float leftEndX = 0 - formationWidth / 2.0f;
        float gapBetweenRibbons = formationWidth / numberOfRibbons;

        for(int i = 0; i < numberOfRibbons; i++) {
            Vector3 initialPosition = new Vector3(leftEndX + i * gapBetweenRibbons, 0, 0);
            this.flightFormationRotatedButNotTranslated.add(initialPosition);

            Vector3 blankPosition = new Vector3(0,0,0);
            this.flightFormationTargetPositionsAfterTranslation.add(blankPosition);
        }
    }
}
