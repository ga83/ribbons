package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by ubuntu on 16/02/19.
 */

public class CircularFlightFormation extends FlightFormation {

    private float FORMATION_RADIUS = 50.0f;

    public CircularFlightFormation() {
        MINIMUM_FORMATIONS = 1;
        MAXIMUM_FORMATIONS = 4;
    }

    @Override
    protected void initFormation() {

        int numberOfRibbons = this.ribbons.size();
        float formationRadius = FORMATION_RADIUS;
        float angleBetweenRibbons = 360.0f / numberOfRibbons;

        for(int i = 0; i < numberOfRibbons; i++) {

            float angleOfRibbon = i * angleBetweenRibbons;
            float x = (float) Math.cos(Math.toRadians(angleOfRibbon)) * formationRadius;
            float y = (float) Math.sin(Math.toRadians(angleOfRibbon)) * formationRadius;

//            Vector3 initialPosition = new Vector3(x, 0f, y);
            Vector3 initialPosition = new Vector3(x, y, 0f);
            this.flightFormationRotatedButNotTranslated.add(initialPosition);

            // flightFormationTargetPositionsAfterTranslation must have as many members as flightFormationRotatedButNotTranslated
            Vector3 blankPosition = new Vector3(0,0,0);
            this.flightFormationTargetPositionsAfterTranslation.add(blankPosition);
        }
    }
}
