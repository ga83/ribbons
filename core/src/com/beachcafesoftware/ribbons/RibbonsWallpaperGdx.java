package com.beachcafesoftware.ribbons;


import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

/*
** Guy Aaltonen
*/
public class RibbonsWallpaperGdx extends ApplicationAdapter {

    static final int SECONDS_PER_BUCKET = 60;

    public static boolean prefsChanged;
    private Preferences prefs;
    private int currentFrame = 0;
    private ArrayList<Formation> activeFormations;
    private Renderer ribbonRenderer;
    private int numObjects;
    static public int UPS = 60;
    private FPSLogger fpsl;
    private long lastTimeBucket = -1;
    private String colorScheme;
    static private String speedSetting;
    private Random rnd = new Random();
    private String imageQuality;
    private String filtering;
    private int diameter;
    private int targetFps;
    private int sleepDuration = 10;
    static private float speedSettingFloat;
    private String ribbonStyle;
    private AppUsageLogger appLogger;


    @Override
    public void create() {
        randomise(true, false,null);  // third parameter is redundant in this case
        fpsl = new FPSLogger();

        appLogger = new GdxWallpaperLogger();
        appLogger.setInterval(60 * 60);
        appLogger.setApplicationName("ribbonsnewinterface");
        appLogger.setUrl("http://159.65.235.251/logappusage.php");

        try {
            appLogger.log();
        } catch (ParametersNotSetException e) {
            e.printStackTrace();
        }

    }

    private void loadPreferences() {
        String numObjectsS;
        String colorSchemeS;
        String speedS;
        String imageQualityS;
        String filteringS;
        String targetFpsS;
        String diameterS;
        String ribbonStyleS;

        prefs = Gdx.app.getPreferences("cube2settings");

        numObjectsS = prefs.getString("numobjects");
        colorSchemeS = prefs.getString("colorscheme");
        speedS = prefs.getString("speed");
        imageQualityS = prefs.getString("imagequality");
        filteringS = prefs.getString("filtering");
        targetFpsS = prefs.getString("targetfps");
        diameterS = prefs.getString("diameter");
        ribbonStyleS = prefs.getString("ribbonstyle");


//        System.out.println("speed in prefs: " + speedS);

        // defaults if empty
        if (numObjectsS.equals("")) {
            prefs.putString("numobjects", "10");
        }

        if (colorSchemeS.equals("")) {
            prefs.putString("colorscheme", "all");
        }

        if (speedS.equals("")) {
            prefs.putString("speed", "normal");
        }

        if(imageQualityS.equals("")) {
            prefs.putString("imagequality","medium");
        }

        if(filteringS.equals(""))		{
            prefs.putString("filtering","bilinear");
        }

        if(targetFpsS.equals("")) {
            prefs.putString("targetfps","30");
        }

        if(diameterS.equals("")) {
            prefs.putString("diameter","30");
        }

        if(ribbonStyleS.equals("")) {
            prefs.putString("ribbonstyle","solarparticles");
        }

        prefs.flush();

        this.ribbonStyle = prefs.getString("ribbonstyle");
        this.numObjects = Integer.parseInt(prefs.getString("numobjects"));
        this.colorScheme = prefs.getString("colorscheme");
        this.speedSetting = prefs.getString("speed");
        this.imageQuality = prefs.getString("imagequality");
        this.filtering = prefs.getString("filtering");
        this.targetFps = Integer.parseInt(prefs.getString("targetfps"));
        this.diameter = Integer.parseInt(prefs.getString("diameter"));


        this.speedSettingFloat = getSpeedAsFloat();


        // stuff below here should not be in loadPreferences, but in randomise (or a method called from there)

        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        /*
        if(ribbonStyle.equals("glowcomet")) {
            ribbonRenderer = new GlowCometRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        */
        /*
        else if(ribbonStyle.equals("spotty")) {
            ribbonRenderer = new SpottyRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        */
        /*
        else if(ribbonStyle.equals("bokehoriginal")) {
            ribbonRenderer = new BokehOriginalLameRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        */
        if(ribbonStyle.equals("orb")) {
            ribbonRenderer = new OrbRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("streamers")) {
            ribbonRenderer = new StreamersRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("shards")) {
            ribbonRenderer = new ShardsRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("wormstreamers")) {
            ribbonRenderer = new WormStreamersRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("transparentwormstreamers")) {
            ribbonRenderer = new TransparentWormStreamersRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("flatwormstreamers")) {
            ribbonRenderer = new FlatWormsStreamersShaderRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("grapes")) {
            ribbonRenderer = new GrapesRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("bokeh")) {
            ribbonRenderer = new BokehRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        /*
        else if(ribbonStyle.equals("discs")) {
            ribbonRenderer = new DiscRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        */
        else if(ribbonStyle.equals("smoke")) {
            ribbonRenderer = new SmokeRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("glowingsmoke")) {
            ribbonRenderer = new GlowingSmokeRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("particles")) {
            ribbonRenderer = new ParticlesRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("glowingparticles")) {
            ribbonRenderer = new GlowingParticlesRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("rocketparticles")) {
            ribbonRenderer = new RocketParticlesRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }
        else if(ribbonStyle.equals("solarparticles")) {
            ribbonRenderer = new SolarParticlesRenderer(targetFps, UPS, imageQuality, filtering, width, height, diameter);
        }

        if(ribbonRenderer == null) {
            System.out.println("rr is null");
        }
        ribbonRenderer.generateBlanker(speedSettingFloat);
    }



    public void randomise(boolean clearAll, boolean changeFormation, Class formationClass) {
        if(clearAll == true) {
            loadPreferences();

            // the only time this is ever reached, we are starting with no activeFormations except the default no-formation
            activeFormations = new ArrayList<Formation>();
            Formation noFormation = new NoFormation();
            noFormation.setSpeed(getSpeed());
            activeFormations.add(noFormation);
            ArrayList<Ribbon> noFormationRibbons = new ArrayList<Ribbon>();
//            System.out.println("stat: adding all ribbons");

            for (int i = 0; i < numObjects; i++) {
                Vector3 velocity = new Vector3();
                float speed = Ribbon.MIN_SPEED + rnd.nextFloat() * (Ribbon.MAX_SPEED - Ribbon.MIN_SPEED);
                velocity.z = speed;
                Vector3 position = new Vector3();
                position.x = Ribbon.BOUNDARY_LEFT + rnd.nextFloat() * (Ribbon.BOUNDARY_RIGHT - Ribbon.BOUNDARY_LEFT);
                position.y = Ribbon.BOUNDARY_TOP + rnd.nextFloat() * (Ribbon.BOUNDARY_BOTTOM - Ribbon.BOUNDARY_TOP);
                position.z = Ribbon.BOUNDARY_FRONT;
                Ribbon r1 = new Ribbon(position, velocity, diameter, colorScheme, getSpeed());

                noFormationRibbons.add(r1);
            }
            noFormation.addRibbons(noFormationRibbons,getSpeed());
        }
        else {  // clearAll == false

            // to test a single particular formation class, UNCOMMENT this
             //if(activeFormations.size() > 1) { return; }

             if(changeFormation == true) {

                if(activeFormations.size() == 1) {
                    int numFormations = 0;
                    try {
                        int minFormations = ((Formation)(formationClass.newInstance())).getMinimumFormations();
                        int maxFormations = ((Formation)(formationClass.newInstance())).getMaximumFormations();
                        numFormations = minFormations + rnd.nextInt(1 + maxFormations - minFormations);
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    int[] numRibbonsPerFormation = new int[numFormations];
                    int ribbonsRemaining = activeFormations.get(0).getRibbons().size();

                    int minRibbonsPerFormation = activeFormations.get(0).getRibbons().size() / numFormations / 2;
                    int maxRibbonsPerFormation = activeFormations.get(0).getRibbons().size() / numFormations;

                    for(int p = 0; p < numFormations; p++) {
                        if(p == numFormations - 1) {
                            numRibbonsPerFormation[p] = ribbonsRemaining;
                        }
                        else {
                            int numberToTake = minRibbonsPerFormation + rnd.nextInt(1 + maxRibbonsPerFormation - minRibbonsPerFormation);

                            // force it to multiple of 2
                            if(numberToTake == 1) {
                                if(rnd.nextBoolean() == true) {
                                    numberToTake = (numberToTake / 2) * 2;  // will reduce 1 to 0
                                }
                                else {
                                    numberToTake = Math.min(2,ribbonsRemaining);       // will increase 1 to 2, unless there are less than 2 left
                                }
                            }
                            else {
                                numberToTake = (numberToTake / 2) * 2;
                            }
                            ribbonsRemaining -= numberToTake;
                            numRibbonsPerFormation[p] = numberToTake;
                        }
                    }

                    for(int h = 0; h < numFormations; h++) {
                        Formation formation = null;
                        try {
                            formation = (Formation) (formationClass.newInstance());
                            formation.setSpeed(getSpeed());
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }

                        // new
//                        System.out.println("loop number " + h + ", adding " + numRibbonsPerFormation[h]);
                        addFormation(formation,numRibbonsPerFormation[h]);
                    }
                }
                else {
                    // remove formations, leaving only the default
                    removeAllFormationsExceptDefault();
                }
            }


            for(int i = 0; i < activeFormations.size(); i++) {
                Formation formation = activeFormations.get(i);
                int changeRibbonIndex = formation.getNextReplacedIndex();

                Vector3 velocity = new Vector3();
                float speed = Ribbon.MIN_SPEED + rnd.nextFloat() * (Ribbon.MAX_SPEED - Ribbon.MIN_SPEED);
                velocity.z = speed;

                Vector3 position = new Vector3();
                position.x = Ribbon.BOUNDARY_LEFT + rnd.nextFloat() * (Ribbon.BOUNDARY_RIGHT  - Ribbon.BOUNDARY_LEFT);
                position.y = Ribbon.BOUNDARY_TOP  + rnd.nextFloat() * (Ribbon.BOUNDARY_BOTTOM - Ribbon.BOUNDARY_TOP);
                position.z = Ribbon.BOUNDARY_FRONT;   // will appear behind the screen and move forward

                if(formation.getRibbons().size() > 0) {
                    Ribbon ribbon = formation.getRibbons().get(changeRibbonIndex);
                    formation.getRibbons().remove(ribbon);
                    formation.getRibbons().add(changeRibbonIndex, new Ribbon(position, velocity, diameter, colorScheme, getSpeed()));

                    // remove from renderer, because some renderers keep a reference to ribbon
                    ribbonRenderer.removeRibbon(ribbon);
                }
            }
        }

    }

    private void addFormation(Formation formation, int size) {
        if(formation == null) {
            formation = new OrbitTheLeaderFormation();
        }

        ArrayList<Ribbon> transferRibbons = new ArrayList<Ribbon>();

        for(int i = 0; i < size; i++) {
            // add to the temp arraylist
            transferRibbons.add(activeFormations.get(0).getRibbons().get(0));

            // remove that ribbon from default
            activeFormations.get(0).getRibbons().remove(0);
        }

        formation.addRibbons(transferRibbons,getSpeed());
        activeFormations.add(formation);
    }

    private void removeAllFormationsExceptDefault() {
        int numFormationsToRemove = activeFormations.size() - 1;
        for(int i = 0; i < numFormationsToRemove; i++) {
            removeFormation(1);
        }
    }

    private void removeFormation(int index) {
        if(index == -1) {
            index = 1 + rnd.nextInt(activeFormations.size() - 1);
        }
        // transfer the ribbons from the removed formation to the default
        ArrayList<Ribbon> ribbons = activeFormations.get(index).getRibbons();
        activeFormations.get(0).addRibbons(ribbons,getSpeed());
//        System.out.println("transferred " + ribbons.size()  + " ribbons");
        activeFormations.get(index).removeAllRibbons();
        activeFormations.remove(index);
    }

    private int shuffledBucket(long bucket,long maxBuckets) {
        if (bucket % 2 == 0) {
            return (int) (bucket / 2);
        }
        else {
            return (int) ((maxBuckets / 2) + ((bucket - 1)/ 2));
        }
    }


    private long getTimeBucket() {
        Calendar c = Calendar.getInstance();
        int utcOffset = c.get(Calendar.ZONE_OFFSET) + c.get(Calendar.DST_OFFSET);
        Long utcMilliseconds = c.getTimeInMillis() + utcOffset;
        long bucket = (utcMilliseconds / (1000 * SECONDS_PER_BUCKET));
        return bucket;
    }


    @Override
    public void render() {

        // TODO: improve the frame limiter. just do it the way games do, if possible.
        // TODO: run as many updates as we should do, based on the current time.
        if(targetFps == 30) {

            if(sleepDuration != 0) {

                try {
                    Thread.sleep(sleepDuration);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            if(Gdx.graphics.getRawDeltaTime() > (1.0f / 29.0f)) {
                sleepDuration -= 1;
                if(sleepDuration < 0) sleepDuration = 0;
            }
            else if(Gdx.graphics.getRawDeltaTime() < (1.0f / 31.0f)) {
                sleepDuration += 1;
            }
        }

        if(currentFrame % 10 == 0) {
            long timeBucket = getTimeBucket();

            if (timeBucket != lastTimeBucket) {
                lastTimeBucket = timeBucket;
                float diceRoll = rnd.nextFloat();
                boolean changeFormation;

               if (activeFormations.size() > 1) { changeFormation = true; }
               else { changeFormation = diceRoll < FormationConstants.CHANGE_FORMATION; }

                Class formationClass = FormationConstants.getRandomFormation(rnd.nextFloat());
                randomise(false, changeFormation, formationClass);
            }
            if (RibbonsWallpaperGdx.prefsChanged == true) {
                prefsChanged = false;
                randomise(true, false, null);
            }
        }

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();


        // need to do a lot of abstraction here to make the renderer class do a lot more and make more decisions
        // eg give its putCloudPuffs method a list of ribbons, not just 1 ribbon
        // this will allow it to do multithreading, decide whether to use a framebuffer (some schemes will not),
        // decide whether to do sorting, etc
        // this may require the renderer to call the formation#updateAllRibbons as well,
        // because for example, although we want to do 720 updates per second,
        // schemes which do NOT blur-putCloudPuffs to a backbuffer will not want to putCloudPuffs after every update
        // nor call the blankout method.
        // we could also handle this by passing putCloudPuffs method a parameter which indicates it is the last
        // update of the frame.
        // if we move the below loop-within-a-loop entirely into the renderer though,
        // i think this gives the most control to the renderer for customisability and optimisation.

        // ORB WORKS WHEN FB.BEGIN AND FB.END ARE COMMENTED OUT. NEED TO MOVE DECISION TO USE FB INTO THE RENDERERS.

        ribbonRenderer.drawAll(width, height, currentFrame, activeFormations);

        currentFrame++;

        fpsl.log();

    }   // /render



    public static float getSpeed() {
        return speedSettingFloat;
    }

    private static float getSpeedAsFloat() {
        if(speedSetting.equals("veryslow")) { return 0.33f; }
        if(speedSetting.equals("slow")) { return 0.66f; }
        if(speedSetting.equals("normal")) { return 1.0f; }
        if(speedSetting.equals("fast")) { return 1.33f; }
        if(speedSetting.equals("veryfast")) { return 1.66f; }

        // ludicrously high dummy value because we shouldn't reach here
        return 10000f;
    }



}
