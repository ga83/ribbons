package com.beachcafesoftware.ribbons;

import com.beachcafesoftware.ribbons.ParametersNotSetException;

/**
 * Created by Guy Aaltonen on 5/09/18.
 */

 // this has been modified without testing...
public interface AppUsageLogger  {

    void log() throws ParametersNotSetException;
    void setUrl(String url);
    void setInterval(int interval); // seconds
    void setApplicationName(String appname);
}
