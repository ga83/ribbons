package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

public class WallpaperTriangle {

    public Vector3 v1;
    public Vector3 v2;
    public Vector3 v3;
    public Color color;
    public Vector3 normal;
    public Texture texture; // only used in schemes that have textures, eg collages


    public WallpaperTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        this.color = color;
        this.normal = Utilities.getScreenFacingNormal(this, null);
    }

    public WallpaperTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color, boolean calculateNormal) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        this.color = color;
    }


    public WallpaperTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color, Vector3 outward) {
        this(v1,v2,v3,color);

        // this overwrites the one in the other constructur, could be improved by bypassing the other one
        this.normal = Utilities.getScreenFacingNormal(this, outward);
    }


    public WallpaperTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color, Texture texture) {
        this(v1,v2,v3,color);
        this.texture = texture;
    }


    public WallpaperTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Color color, Vector3 outward, Texture texture) {
//        this(v1,v2,v3,color);
        this(v1,v2,v3,color,texture);

        // this overwrites the one in the other constructur, could be improved by bypassing the other one
        this.normal = Utilities.getScreenFacingNormal(this, outward);
    }


}
