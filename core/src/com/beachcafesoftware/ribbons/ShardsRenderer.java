package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ImmediateModeRenderer20;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Guy Aaltonen on 5/01/19.
 */

class Rung {
    Vector3 rungCentrePosition;
    Vector3 rungDirection;
}

// TODO: touch up logic to render a ribbon even if it's not active anymore. as of now, when a ribbon is replaced,
// TODO: the whole thing disappears at once. it would be nice for it to shrink and then disappear.
// TODO: ribbon only gets replaced every 90 seconds, so it's not urgent, but it would look better.
public class ShardsRenderer implements Renderer {

    private static float RIBBON_WIDTH;
    private static final float RIBBON_MAX_RUNGS = 10.0f;
    private final int UPS;
    private final int targetFps;

    PerspectiveCamera cam;

    ImmediateModeRenderer20 renderer;
    static final float[] currentTriangle = new float[9];
    float color;

    static Vector3 p1;
    static Vector3 p2;
    static Vector3 p3;
    static Vector3 p4;


    Map<Ribbon,ArrayList<Rung>> rungMap = new HashMap<Ribbon, ArrayList<Rung>>();

    @Override
    public void drawAll(int width, int height, int currentFrame, ArrayList<Formation> activeFormations) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        cam.update();
        renderer.begin(cam.combined, GL20.GL_TRIANGLES);

        float delta = (1f / UPS);
        for(int k = 0; k * targetFps < UPS; k++) {
            for(int i = 0; i < activeFormations.size(); i++) {
                activeFormations.get(i).updateAllRibbons((float)delta);
            }
        }

        for(int i = 0; i < activeFormations.size(); i++) {
            ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

            for(int j = 0; j < formationRibbons.size(); j++) {
                draw(formationRibbons.get(j));
            }
        }

        for(int i = 0; i < activeFormations.size(); i++) {
            ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

            for (int j = 0; j < formationRibbons.size(); j++) {
                Ribbon ribbon = formationRibbons.get(j);
                ArrayList rungList = rungMap.get(ribbon);
                int rungListLength;
                rungListLength = rungList.size();

                if (
                        rungListLength % 2 == 0 &&
                        rungListLength > (RIBBON_MAX_RUNGS * (targetFps / 30.0f) )
                )

                {
                    rungList.remove(0);
                    rungList.remove(0);
                    rungListLength = rungList.size();
                }

                for (int k = 1; k < rungListLength; k++) {
                    Rung frontRung = rungMap.get(ribbon).get(k);
                    Rung rearRung  = rungMap.get(ribbon).get(k - 1);

                    float frontRungLength = (rungListLength / RIBBON_MAX_RUNGS) * RIBBON_WIDTH;
                    float rearRungLength  = (rungListLength / RIBBON_MAX_RUNGS) * RIBBON_WIDTH;

                    float period = rungListLength * 2;

                    float frontPeriodicComponent = (float) Math.sin((k / period) * (2 * Math.PI));
                    float rearPeriodicComponent = (float) Math.sin(((k - 1) / period) * (2 * Math.PI));

                    float closeness = 1f / frontRung.rungCentrePosition.z;
                    float lum = Math.min(closeness * 200 , 1.0f);

                    float r = Math.min(ribbon.r * 2000 * lum, 1.0f);
                    float g = Math.min(ribbon.g * 2000 * lum, 1.0f);
                    float b = Math.min(ribbon.b * 2000 * lum, 1.0f);

                    float ribbonColor = com.badlogic.gdx.graphics.Color.toFloatBits(r, g, b, 1.0f);

                    p1 = frontRung.rungCentrePosition.cpy().add(frontRung.rungDirection.cpy().scl( (frontRungLength * frontPeriodicComponent)));
                    p2 = frontRung.rungCentrePosition.cpy().add(frontRung.rungDirection.cpy().scl( (frontRungLength * frontPeriodicComponent * -1.0f)));
                    p3 = rearRung.rungCentrePosition.cpy() .add(rearRung .rungDirection.cpy().scl( (rearRungLength  * rearPeriodicComponent)));
                    p4 = rearRung.rungCentrePosition.cpy() .add(rearRung .rungDirection.cpy().scl( (rearRungLength  * rearPeriodicComponent * -1.0f)));

                    currentTriangle[0] = p1.x;
                    currentTriangle[1] = p1.y;
                    currentTriangle[2] = p1.z;
                    currentTriangle[3] = p2.x;
                    currentTriangle[4] = p2.y;
                    currentTriangle[5] = p2.z;
                    currentTriangle[6] = p3.x;
                    currentTriangle[7] = p3.y;
                    currentTriangle[8] = p3.z;
                    color = ribbonColor;
                    drawTriangle();

                    currentTriangle[0] = p3.x;
                    currentTriangle[1] = p3.y;
                    currentTriangle[2] = p3.z;
                    currentTriangle[3] = p2.x;
                    currentTriangle[4] = p2.y;
                    currentTriangle[5] = p2.z;
                    currentTriangle[6] = p4.x;
                    currentTriangle[7] = p4.y;
                    currentTriangle[8] = p4.z;
                    color = ribbonColor;
                    drawTriangle();

                }
            }
        }
        renderer.end();
    }

    public void draw(Ribbon ribbon) {
        if(rungMap.get(ribbon) == null) {
            rungMap.put(ribbon, new ArrayList<Rung>());
        }

        Rung rung;
        rung = new Rung();
        rung.rungCentrePosition = ribbon.position.cpy();
        rung.rungDirection = ribbon.rotationAxis.cpy();
        rungMap.get(ribbon).add(rung);
    }

    void drawTriangle() {
        renderer.color(color);
        renderer.vertex(currentTriangle[0], currentTriangle[1], currentTriangle[2]);
        renderer.color(color);
        renderer.vertex(currentTriangle[3], currentTriangle[4], currentTriangle[5]);
        renderer.color(color);
        renderer.vertex(currentTriangle[6], currentTriangle[7], currentTriangle[8]);
    }

    @Override
    public void generateBlanker(float speedSetting) {

    }

    @Override
    public void blankOut(SpriteBatch sb, int width, int height) {

    }

    @Override
    public void removeRibbon(Ribbon r) {
        // TODO: because we keep references to ribbons in this renderer, remove our own reference.
        // TODO: however, we should still putCloudPuffs the "expired" ribbon, and reduce its size,
        // TODO: until there is none left. it looks better than removing the whole thing at once.
        // TODO: this is a temporary hack.
        // TODO: probably move the ribbon into a temporary list for expired ribbons.
        // TODO: and of course render those, and reduce their size each frame.
        // TODO: ribbons get replaced every 90 seconds so it's not a huge deal.
        // TODO: but it would still be preferable.
        this.rungMap.remove(r);

    }

    public ShardsRenderer(int targetFps, int UPS, String imageQuality, String filtering, int width, int height, int diameter) {
        cam = new PerspectiveCamera(90, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.x = 0;
        cam.position.y = 0;
        cam.position.z = 0;
        cam.lookAt(0,0,1f);
        cam.near = 0.1f;   // must be greater than 0 otherwise depth test fails frequently
        cam.far = 6000;

        renderer = new ImmediateModeRenderer20(200000,false, true, 0);

        this.targetFps = targetFps;
        this.UPS = UPS;

        RIBBON_WIDTH = 4.0f * diameter / 60.0f;

        Gdx.gl20.glEnable(GL20.GL_DEPTH_TEST);
        Gdx.gl20.glDepthMask(true);

        Gdx.gl.glClearDepthf(6000f);
        Gdx.gl.glDepthFunc(GL20.GL_LESS);
        Gdx.gl.glDepthRangef(-6000f, 6000f);

        Gdx.gl20.glDisable(GL20.GL_BLEND);

    }

}
