package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 5/01/19.
 */

// TODO: touch up logic to render a ribbon even if it's not active anymore. as of now, when a ribbon is replaced,
// TODO: the whole thing disappears at once. it would be nice for it to shrink and then disappear.
// TODO: ribbon only gets replaced every 90 seconds, so it's not urgent, but it would look better.
public class BokehRenderer extends FrameBufferBasedRenderer implements Renderer {

    public static final float SPRITE_LIFE = 15.0f;
    private ArrayList<SpriteLifeColor> discs = new ArrayList<SpriteLifeColor>();
    private final int UPS;
    private final int targetFps;
    float color;
    SpriteBatch sb;
    static float MAX_OFFSET_FACTOR = 3.0f;  // multiplier of diameter
    Texture star;
    static float[] points = new float[20];
    private int RIBBON_DIAMETER;
    int a = 0;
    private Random rnd = new Random();
    private boolean putSparkle;


    @Override
    public void drawAll(int width, int height, int currentFrame, ArrayList<Formation> activeFormations) {

        for (int k = 0; k * targetFps < UPS; k++) {

            // looks good if we only do it every second position update
            putSparkle = !putSparkle;

            float delta = (1f / UPS);

            for (int i = 0; i < activeFormations.size(); i++) {
                activeFormations.get(i).updateAllRibbons(delta);

                ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

                if(putSparkle == true) {
                    for (int j = 0; j < formationRibbons.size(); j++) {
                        putSparkles(formationRibbons.get(j));
                    }
                }
            }
        }

        drawCloudPuffs(currentFrame, width, height);
    }

    private void drawCloudPuffs(int currentFrame, int width, int height) {

        if (currentFrame % targetFps * 4 == 0) {
            sb.dispose();
            sb = new SpriteBatch();
        }

        // draw ribbons onto framebuffer
        fb.begin();

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sb.begin();

        sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);

        // remove dead puffs
        int c = 0;

        if(targetFps == 30 || (currentFrame % 2 == 0)) {
            while (c < discs.size()) {

                SpriteLifeColor sparkle = discs.get(c);
                sparkle.life -= 0.1;

                if (sparkle.life < 0) {
                    discs.remove(c);
                } else {
                    c++;
                }
            }
        }

        for(int i = 0; i < discs.size(); i++) {
            SpriteLifeColor sparkle = discs.get(i);

            float reciprocalOfZ = 1f / sparkle.position.z;
            int shortSideLength = Math.min(width,height);

            float period = SPRITE_LIFE * 2;

            float frontPeriodicComponent = (float) Math.sin((sparkle.life / period) * (2 * Math.PI));

            float diameterSmall = ((RIBBON_DIAMETER * shortSideLength) * reciprocalOfZ) * frontPeriodicComponent;

            float colori = sparkle.color;
            float dxh = sparkle.position.x;
            float dyh = sparkle.position.y;
            float xc = (width * 0.5f)  + ((dxh) * reciprocalOfZ) * shortSideLength;
            float yc = (height * 0.5f) + ((dyh) * reciprocalOfZ) * shortSideLength;

            float x1 = xc - diameterSmall * 0.5f;
            float y1 = yc - diameterSmall * 0.5f;
            float x2 = x1 + diameterSmall;
            float y2 = y1 + diameterSmall;

            points[0] = x1 ;
            points[1] = y1 ;
            points[2] = colori;
            points[5] = x2 ;
            points[6] = y1 ;
            points[7] = colori;
            points[10] = x2 ;
            points[11] = y2 ;
            points[12] = colori;
            points[15] = x1 ;
            points[16] = y2 ;
            points[17] = colori;

            sb.draw(star, points, 0, 20);
        }

        sb.end();
        fb.end();

        // draw framebuffer onto screen
        sb.begin();
        sb.draw(fb.getColorBufferTexture(), 0f, 0f, width, height);
        sb.end();

    }

    public void putSparkles(Ribbon ribbon) {
        putSparkle(ribbon);
    }


    public void putSparkle(Ribbon ribbon) {
        if(ribbon.position.z <= 0) { return; }   // near z clipping

//        Vector3 randomisedPosition = ribbon.position.cpy();
  //      float alphaFactor = 0.1f;

        Vector3 randomOffset = new Vector3().setToRandomDirection().scl(ribbon.diameter * MAX_OFFSET_FACTOR * rnd.nextFloat());
        Vector3 randomisedPosition = ribbon.position.cpy().add(randomOffset);

        float alphaFactor = 0.65f;


        float colori = com.badlogic.gdx.graphics.Color.toFloatBits(
                ribbon.r * 2000 * 2, ribbon.g * 2000 * 2, ribbon.b * 2000 * 2, alphaFactor
        );


        SpriteLifeColor puff = new SpriteLifeColor();
        puff.position = randomisedPosition;

        puff.life = SPRITE_LIFE;

        puff.color = colori;

        discs.add(puff);
    }

    @Override
    public void generateBlanker(float speedSetting) {

    }

    @Override
    public void blankOut(SpriteBatch sb, int width, int height) {

    }

    @Override
    public void removeRibbon(Ribbon r) {
        // TODO: because we keep references to ribbons in this renderer, remove our own reference.
        // TODO: however, we should still putCloudPuffs the "expired" ribbon, and reduce its size,
        // TODO: until there is none left. it looks better than removing the whole thing at once.
        // TODO: this is a temporary hack.
        // TODO: probably move the ribbon into a temporary list for expired ribbons.
        // TODO: and of course render those, and reduce their size each frame.
        // TODO: ribbons get replaced every 90 seconds so it's not a huge deal.
        // TODO: but it would still be preferable.
    }

    public BokehRenderer(int targetFps, int UPS, String imageQuality, String filtering, int width, int height, int diameter) {
        String prefix = "";
        String filepath = new String(prefix + "glow7" + ".png");
        FileHandle handle = Gdx.files.internal(filepath);
        star = new Texture(handle, Pixmap.Format.RGBA8888, false);

        this.targetFps = targetFps;
        this.UPS = UPS;

        this.initialiseFrameBuffer(imageQuality, width, height);

        this.sb = new SpriteBatch();

        Texture.TextureFilter filter = filtering.equals("nearest") ? Texture.TextureFilter.Nearest : Texture.TextureFilter.Linear;
        fb.getColorBufferTexture().setFilter(filter,filter);

        RIBBON_DIAMETER = (int) (diameter * 1.5);

        Gdx.gl20.glDisable(GL20.GL_DEPTH_TEST);

        points[3] = 0;
        points[4] = 0;
        points[8] = 1;
        points[9] = 0;
        points[13] = 1;
        points[14] = 1;
        points[18] = 0;
        points[19] = 1;
    }

}
