package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by Guy Aaltonen on 27/01/19.
 */

public class Particle {
    Vector3 position;
    Vector3 velocity;
    float life;
    float color;
}
