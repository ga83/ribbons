package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;


/**
 * Created by Guy Aaltonen on 5/01/19.
 */
public class GrapesRenderer extends FrameBufferBasedRenderer implements Renderer  {

    public static final float SPRITE_LIFE = 6.0f;
    private ArrayList<SpriteLifeColor> puffs = new ArrayList<SpriteLifeColor>();
    static float MAX_OFFSET_FACTOR = 0.9f;  // multiplier of diameter
    private final int UPS;
    private final int targetFps;
    static Texture glow;
    static float[] points = new float[20];
    SpriteBatch sb;
    Random rnd = new Random();
    private int RIBBON_DIAMETER;
    private GrapePuffComparator puffComparator = new GrapePuffComparator();


    @Override
    public void drawAll(int width, int height, int currentFrame, ArrayList<Formation> activeFormations) {


        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            for (int k = 0; k * targetFps < UPS; k++) {

                float delta = (1f / UPS);

                sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
                for (int i = 0; i < activeFormations.size(); i++) {

                    activeFormations.get(i).updateAllRibbons((float) delta);

                    ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

                    for (int j = 0; j < formationRibbons.size(); j++) {
                        putCloudPuffs(formationRibbons.get(j), sb, width, height);
                    }
                }
            }

        sortCloudPuffs();

        drawCloudPuffs(currentFrame, width, height);
    }

    private void sortCloudPuffs() {
        puffs.sort(puffComparator);
    }

    private void drawCloudPuffs(int currentFrame, int width, int height) {

        if (currentFrame % targetFps * 4 == 0) {
            sb.dispose();
            sb = new SpriteBatch();
        }

        // draw ribbons onto framebuffer
        fb.begin();

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sb.begin();

        // best blending function for general transparency
        sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        // remove dead puffs
        int c = 0;

        if(targetFps == 30 || (currentFrame % 2 == 0)) {
            while (c < puffs.size()) {

                SpriteLifeColor puff = puffs.get(c);
                puff.life -= 0.1;

                if (puff.life < 0) {
                    puffs.remove(c);
                } else {
                    c++;
                }
            }
        }

        for(int i = 0; i < puffs.size(); i++) {

            SpriteLifeColor puff = puffs.get(i);
            float colori = puff.color;
            //puff.life -= 0.1;

            float reciprocalOfZ = 1f / puff.position.z;
            int shortSideLength = Math.min(width,height);

//            float diameterSmall = ((RIBBON_DIAMETER * shortSideLength) * reciprocalOfZ) * rnd.nextFloat() * 0.5f;
            float diameterSmall = ((RIBBON_DIAMETER * shortSideLength) * reciprocalOfZ) * 0.5f;
            float dxh = puff.position.x;
            float dyh = puff.position.y;
            float xc = (width * 0.5f)  + ((dxh) * reciprocalOfZ) * shortSideLength;
            float yc = (height * 0.5f) + ((dyh) * reciprocalOfZ) * shortSideLength;

            float x1 = xc - diameterSmall * 0.5f;
            float y1 = yc - diameterSmall * 0.5f;
            float x2 = x1 + diameterSmall;
            float y2 = y1 + diameterSmall;

            points[0] = x1 ;
            points[1] = y1 ;
            points[2] = colori;
            points[5] = x2 ;
            points[6] = y1 ;
            points[7] = colori;
            points[10] = x2 ;
            points[11] = y2 ;
            points[12] = colori;
            points[15] = x1 ;
            points[16] = y2 ;
            points[17] = colori;

            sb.draw(glow, points, 0, 20);
        }

        sb.end();
        fb.end();

        // draw framebuffer onto screen
        sb.begin();
        sb.draw(fb.getColorBufferTexture(),0f,0f,width,height);
        sb.end();

    }


    public void putCloudPuffs(Ribbon ribbon, SpriteBatch sb, int width, int height) {
        putCloudPuff(ribbon, sb, width, height);
//        putCloudPuff(ribbon, sb, width, height);
  //      putCloudPuff(ribbon, sb, width, height);
    }


    public void putCloudPuff(Ribbon ribbon, SpriteBatch sb, int width, int height) {
        // even though technically the drawing position is determined after adding the offset, clipping now saves a lot of
        // cpu cycles. we could probably recover more cpu cycles with even better clipping logic. do this some time...
        if(ribbon.position.z <= 0) { return; }   // near z clipping

        Vector3 randomOffset = new Vector3().setToRandomDirection().scl(ribbon.diameter * MAX_OFFSET_FACTOR * rnd.nextFloat());
        Vector3 randomisedPosition = ribbon.position.cpy().add(randomOffset);

        float alphaFactor = 0.65f - randomisedPosition.z * 0.001f * 0.65f;

        if(alphaFactor < 0.0f) {
            alphaFactor = 0.0f;
        }

        alphaFactor = 1.0f;

        float colori = com.badlogic.gdx.graphics.Color.toFloatBits(
                ribbon.r * 2000 * 2, ribbon.g * 2000 * 2, ribbon.b * 2000 * 2, alphaFactor
        );

        SpriteLifeColor puff = new SpriteLifeColor();
        puff.position = randomisedPosition;
        puff.life = SPRITE_LIFE;
        puff.color = colori;

        puffs.add(puff);
    }


    @Override
    public void generateBlanker(float speedSetting) {

    }

    @Override
    public void blankOut(SpriteBatch sb, int width, int height) {

    }

    @Override
    public void removeRibbon(Ribbon r) {
        // i don't think we need to hold references to ribbons in this class,
        // but if so, remember to release them here
    }

    // TODO: replace the alpha-fading glow with white-to-black, with hard transparency cliff edge around "grape".
    public GrapesRenderer(int targetFps, int UPS, String imageQuality, String filtering, int width, int height, int diameter) {

        String prefix = "";
        String filepath = new String(prefix + "glow5" + ".png");
        FileHandle handle = Gdx.files.internal(filepath);
        glow = new Texture(handle, Pixmap.Format.RGBA8888, false);

        this.targetFps = targetFps;
        this.UPS = UPS;

        this.initialiseFrameBuffer(imageQuality, width, height);

        this.sb = new SpriteBatch();

        Texture.TextureFilter filter = filtering.equals("nearest") == true ? Texture.TextureFilter.Nearest : Texture.TextureFilter.Linear;
        fb.getColorBufferTexture().setFilter(filter,filter);

        RIBBON_DIAMETER = diameter;

        Gdx.gl20.glDisable(GL20.GL_DEPTH_TEST);

        points[3] = 0;
        points[4] = 0;
        points[8] = 1;
        points[9] = 0;
        points[13] = 1;
        points[14] = 1;
        points[18] = 0;
        points[19] = 1;
    }

}

