package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by ubuntu on 16/02/19.
 */

public class ChevronFlightFormation extends FlightFormation {

    public ChevronFlightFormation() {
        MINIMUM_FORMATIONS = 1;
        MAXIMUM_FORMATIONS = 4;
    }

    @Override
    protected void initFormation() {

        int numberOfRibbons = this.ribbons.size();
        float formationWidth = 100.0f;
        float leftEndX = 0 - (formationWidth / 2.0f);
        float gapBetweenRibbons = formationWidth / numberOfRibbons;
        float centre = formationWidth / 2.0f;

        for(int i = 0; i < numberOfRibbons; i++) {
            float xRelative = i * gapBetweenRibbons + gapBetweenRibbons * 0.5f;
            float x = leftEndX + xRelative;
            float z = -Math.abs(xRelative - centre);

            Vector3 initialPosition = new Vector3(x, 0, z);
            this.flightFormationRotatedButNotTranslated.add(initialPosition);

            Vector3 blankPosition = new Vector3(0,0,0);
            this.flightFormationTargetPositionsAfterTranslation.add(blankPosition);
        }
    }
}
