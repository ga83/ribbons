package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ImmediateModeRenderer20;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Guy Aaltonen on 5/01/19.
 */

class Triangle {
    float[] v1 = new float[3];
    float[] v2 = new float[3];
    float[] v3 = new float[3];
    float color;
}

// TODO: touch up logic to render a ribbon even if it's not active anymore. as of now, when a ribbon is replaced,
// TODO: the whole thing disappears at once. it would be nice for it to shrink and then disappear.
// TODO: ribbon only gets replaced every 90 seconds, so it's not urgent, but it would look better.
public class StreamersRenderer implements Renderer {

    private static float RIBBON_WIDTH = 4.0f;
    private static final int RIBBON_LENGTH = 300;
    private final int UPS;
    private final int targetFps;

    /*
    private final ShaderProgram shaderProgram = null;
    private final Model model;
    private final ModelInstance instance;
    private final ModelBatch modelBatch;
    private final Environment environment;
    private final Mesh mesh = null;
    */

    PerspectiveCamera cam;

    ImmediateModeRenderer20 renderer;
    static final float[] currentTriangle = new float[9];
    static float color;

    Map<Ribbon,ArrayList<Triangle>> triangleMap = new HashMap<Ribbon, ArrayList<Triangle>>();

    @Override
    public void drawAll(int width, int height, int currentFrame, ArrayList<Formation> activeFormations) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        cam.update();
        renderer.begin(cam.combined, GL20.GL_TRIANGLES);

        float delta = (1f / UPS);
        for(int k = 0; k * targetFps < UPS; k++) {
            for(int i = 0; i < activeFormations.size(); i++) {
                activeFormations.get(i).updateAllRibbons((float)delta);
            }
        }

        for(int i = 0; i < activeFormations.size(); i++) {
            ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

            for(int j = 0; j < formationRibbons.size(); j++) {
                draw(formationRibbons.get(j));
            }
        }

        for(int i = 0; i < activeFormations.size(); i++) {
            ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

            for (int j = 0; j < formationRibbons.size(); j++) {
                ArrayList triangleList = triangleMap.get(formationRibbons.get(j));
                int triangleListLength;
                triangleListLength = triangleList.size();

                if (
                        triangleListLength % 2 == 0 &&
                        triangleListLength > (RIBBON_LENGTH * (targetFps / 30.0f) )
                        )

                {
                    triangleList.remove(0);
                    triangleList.remove(0);
                    triangleListLength = triangleList.size();
                }

                for (int k = 0; k < triangleListLength; k++) {
                    Triangle triangle = triangleMap.get(formationRibbons.get(j)).get(k);

                    currentTriangle[0] = triangle.v1[0];
                    currentTriangle[1] = triangle.v1[1];
                    currentTriangle[2] = triangle.v1[2];
                    currentTriangle[3] = triangle.v2[0];
                    currentTriangle[4] = triangle.v2[1];
                    currentTriangle[5] = triangle.v2[2];
                    currentTriangle[6] = triangle.v3[0];
                    currentTriangle[7] = triangle.v3[1];
                    currentTriangle[8] = triangle.v3[2];
                    color = triangle.color;

                    drawTriangle();
                }
            }
        }

         renderer.end();
    }


    void drawTriangle() {

        // every 3 calls to vertex completes a triangle
        renderer.color(color);
        renderer.vertex(currentTriangle[0], currentTriangle[1], currentTriangle[2]);
        renderer.color(color);
        renderer.vertex(currentTriangle[3], currentTriangle[4], currentTriangle[5]);
        renderer.color(color);
        renderer.vertex(currentTriangle[6], currentTriangle[7], currentTriangle[8]);
    }




    public void draw(Ribbon ribbon) {

        // set color
        float closeness = 1f / (ribbon.position.z / 4.0f);
        float lum = Math.min(closeness * 200 , 1.0f);

        float r = Math.min(ribbon.r * 2000 * lum, 1.0f);
        float g = Math.min(ribbon.g * 2000 * lum, 1.0f);
        float b = Math.min(ribbon.b * 2000 * lum, 1.0f);

        float color = com.badlogic.gdx.graphics.Color.toFloatBits(
                r, g, b, 1f
        );

        Vector3 centre = ribbon.position.cpy();

        Vector3 corner1 = centre.cpy().add(ribbon.rotationAxis.cpy().scl(RIBBON_WIDTH));
        Vector3 corner2 = centre.cpy().add(ribbon.rotationAxis.cpy().scl(-RIBBON_WIDTH));

        Vector3 corner3 = centre.cpy().add(ribbon.rotationAxis.cpy().scl( RIBBON_WIDTH)).add(ribbon.velocity.cpy().scl(-.03f));
        Vector3 corner4 = centre.cpy().add(ribbon.rotationAxis.cpy().scl(-RIBBON_WIDTH)).add(ribbon.velocity.cpy().scl(-.03f));


        if(triangleMap.get(ribbon) == null) {
            triangleMap.put(ribbon, new ArrayList<Triangle>());
        }

        int triSize = triangleMap.get(ribbon).size();

        if(triSize == 1) {
            int triLast = 0;
            corner3 = new Vector3(triangleMap.get(ribbon).get(triLast).v1);
            corner4 = new Vector3(triangleMap.get(ribbon).get(triLast).v2);
        }
        else if(triSize > 1) {
            int triLast = triangleMap.get(ribbon).size() - 2;
            corner3 = new Vector3(triangleMap.get(ribbon).get(triLast).v1);
            corner4 = new Vector3(triangleMap.get(ribbon).get(triLast).v2);
        }

        /*
        1 2
        3 4
         */
        Triangle s;


        s = new Triangle();
        s.v1 = new float[] { corner1.x, corner1.y, corner1.z };
        s.v2 = new float[] { corner2.x, corner2.y, corner2.z };
        s.v3 = new float[] { corner3.x, corner3.y, corner3.z };
        s.color = color;
        triangleMap.get(ribbon).add(s);

        s = new Triangle();
        s.v1 = new float[] { corner3.x, corner3.y, corner3.z };
        s.v2 = new float[] { corner2.x, corner2.y, corner2.z };
        s.v3 = new float[] { corner4.x, corner4.y, corner4.z };
        s.color = color;
        triangleMap.get(ribbon).add(s);
    }

    @Override
    public void generateBlanker(float speedSetting) {

    }

    @Override
    public void blankOut(SpriteBatch sb, int width, int height) {

    }

    @Override
    public void removeRibbon(Ribbon r) {
        // TODO: because we keep references to ribbons in this renderer, remove our own reference.
        // TODO: however, we should still putCloudPuffs the "expired" ribbon, and reduce its size,
        // TODO: until there is none left. it looks better than removing the whole thing at once.
        // TODO: this is a temporary hack.
        // TODO: probably move the ribbon into a temporary list for expired ribbons.
        // TODO: and of course render those, and reduce their size each frame.
        // TODO: ribbons get replaced every 90 seconds so it's not a huge deal.
        // TODO: but it would still be preferable.
        this.triangleMap.remove(r);

    }

    public StreamersRenderer(int targetFps, int UPS, String imageQuality, String filtering, int width, int height, int diameter) {
//        cam = new PerspectiveCamera(90, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam = new PerspectiveCamera(150, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.x = 0;
        cam.position.y = 0;
        cam.position.z = 0;
        cam.lookAt(0,0,1f);
        cam.near = 1;   // must be greater than 0 otherwise depth test fails frequently
        cam.far = 6000;
//        cam.update();

        renderer = new ImmediateModeRenderer20(200000,false, true, 0);

        this.targetFps = targetFps;
        this.UPS = UPS;

        RIBBON_WIDTH = 4.0f * diameter / 60.0f;

        Gdx.gl20.glEnable(GL20.GL_DEPTH_TEST);
        Gdx.gl20.glDepthMask(true);

        Gdx.gl.glClearDepthf(6000f);
        Gdx.gl.glDepthFunc(GL20.GL_LESS);
        Gdx.gl.glDepthRangef(-6000f, 6000f);

        Gdx.gl20.glDisable(GL20.GL_BLEND);



    }


}

