package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public class ElectronShellsNonCoPlanar extends TransformingFormation {

    public static final float MIN_RIBBON_AXIS_ROTATION_RANDOMISATION_CHANGE_RATE = 1.0f;
    public static final float MAX_RIBBON_AXIS_ROTATION_RANDOMISATION_CHANGE_RATE = 20.0f;
    static float BOUNDARY_RIGHT =  1.0f + 450.0f;
    static float BOUNDARY_LEFT =   0.0f - 450.0f;
    static float BOUNDARY_BOTTOM = 1.0f + 450.0f;
    static float BOUNDARY_TOP =    0.0f - 450.0f;
    static float BOUNDARY_FRONT =  300.0f;
    static float BOUNDARY_BACK =   600.0f;


    protected static final float LEADER_MAX_SPEED = 50.0f;
    protected static final float LEADER_MIN_SPEED = 25.0f;

    static private float HEIGHT_PER_SHELL_LEVEL = 25.0f;
    static private int MAX_SHELLS = 3;
    private ArrayList<Vector3> rotationAxes; // the directions of the rotation axes
    private ArrayList<Float> rotationAxisRotationRates; // the rates of modification of rotation axes

    static private float rotationRate = 270.0f; // the rotation rate of the electron around the nucleus. same for all ribbons across all formation instances.


    public ElectronShellsNonCoPlanar() {
        MINIMUM_FORMATIONS = 1;
        MAXIMUM_FORMATIONS = 4;

        leaderSpeed = LEADER_MIN_SPEED;

        formationCentrePosition = new Vector3(
                -300 + rnd.nextFloat() * 600,
                -300 + rnd.nextFloat() * 600,
                400 + rnd.nextFloat() * 500
        );
    }

    @Override
    public void setSpeed(float speed) {
        this.speedSettingFloat = speed;
        float leaderSpeed = LEADER_MIN_SPEED + rnd.nextFloat() * (LEADER_MAX_SPEED - LEADER_MIN_SPEED);
        this.formationCentreVelocity = new Vector3().setToRandomDirection().scl(leaderSpeed);
    }


    @Override
    protected void initFormation() {
        int numberOfRibbons = this.ribbons.size();

        this.rotationAxes = new ArrayList<Vector3>();
        this.rotationAxisRotationRates = new ArrayList<Float>();

        for(int i = 0; i < numberOfRibbons; i++) {

            // electron heights are discrete
            int shell = getShellLevel();
            float electronHeight = shell * HEIGHT_PER_SHELL_LEVEL;

            // rotation axis must be perpendicular to the plane of travel, so create both, and then...
            Vector3 rotationAxis = new Vector3(1,0,0);
            Vector3 initialPosition = new Vector3(0,1,0).scl(electronHeight);

            // ... and then, rotate them together so they stay in sync, ie, 90 degrees to each other, still.
            Vector3 randomRotationAxis = new Vector3().setToRandomDirection();
            float randomRotationAmount = rnd.nextFloat() * 360.0f;
            rotationAxis.rotate(randomRotationAxis, randomRotationAmount);
            initialPosition.rotate(randomRotationAxis, randomRotationAmount);

            // add rotation axis to list
            rotationAxes.add(rotationAxis);

            // add rotation rate to list
            float rotationAxisRotationRate = (MIN_RIBBON_AXIS_ROTATION_RANDOMISATION_CHANGE_RATE + rnd.nextFloat() * MAX_RIBBON_AXIS_ROTATION_RANDOMISATION_CHANGE_RATE) / 60.0f;
            rotationAxisRotationRates.add(rotationAxisRotationRate);

            Vector3 position = this.ribbons.get(i).position.cpy();
            Vector3 velocity = this.ribbons.get(i).velocity.cpy();
            float diameter = this.ribbons.get(i).diameter;
            String scheme = "all";
            float speedMult = 1;

            Ribbon ribbonCopy = new Ribbon(position,velocity,diameter,scheme,speedMult);
            ribbonCopy.rotationAxis = ribbonCopy.rotationAxis.cpy();
            ribbonCopy.position = initialPosition;

            this.flightFormationInitial.add(ribbonCopy);
            this.flightFormationTargetPositions.add(ribbonCopy);

        }
    }


    // for now, just random between 1 and 4. do not follow real atoms. TODO: maybe make the numbers per level more realistic.
    private int getShellLevel() {
        return 1 + rnd.nextInt(MAX_SHELLS);
 //       return 3;
    }


    @Override
    public void updateAllRibbons(float dt) {

        // all of the rotation and scaling happens in here... a LOT happens in here
        updateFormation(dt);

        // set the target position of every ribbon, and then move them towards it, or just move it directly if locked already
        for(int i = 0; i < ribbons.size(); i++) {
            // ribbons list is corresponding to flightFormationInitial list
            Ribbon ribbon = ribbons.get(i);
            Ribbon ribbonTargetPosition = this.flightFormationTargetPositions.get(i);
            Vector3 oldPosition = ribbon.position.cpy();
            Vector3 towardsTarget = ribbonTargetPosition.position.cpy().sub(ribbon.position);

            // guyy debug
            locked[i] = true;

            // copy
            if(locked[i] == true) {
                ribbon.position.x = ribbonTargetPosition.position.x;
                ribbon.position.y = ribbonTargetPosition.position.y;
                ribbon.position.z = ribbonTargetPosition.position.z;



                // TODO
                // probably have to have some ribbon.rotationaxis calculation around here for the streamersrenderer
                // AND SET r.rotationAxis HERE!
                // r.rotationAxis = f.rotationAxis; // ?? ... cpy??
            }
            else {
/*
                System.out.println("ii: xx");

                // steer towards target
                Vector3 towardsTargetVelocity = towardsTarget.cpy().nor().scl(approachSpeed * speedSettingFloat);
                if(towardsTargetVelocity.len() * dt > towardsTarget.len()) {
                    locked[i] = true;
                }
                else {
                    ribbon.velocity.add(towardsTargetVelocity);
                    ribbon.velocity.nor().scl(approachSpeed * speedSettingFloat);
                    ribbon.updatePosition(dt);
                }
                */
            }

            // for color calculation
            float distanceTravelled = oldPosition.cpy().dst(ribbon.position);
            float ribbonSpeed = distanceTravelled / dt;

            ribbon.speed = ribbonSpeed;
            ribbon.velocity.nor().scl(ribbonSpeed);

            approachSpeed *= 1.000001f;
        }
    }


    private void updateFormation(float dt) {

        // integrate velocity
        formationCentrePosition.x += formationCentreVelocity.x * dt;
        formationCentrePosition.y += formationCentreVelocity.y * dt;
        formationCentrePosition.z += formationCentreVelocity.z * dt;

        // randomise the axis for every ribbon, must randomise the position of the ribbon itself, and also the axis
        if(rnd.nextFloat() < 1f) {

            for(int i = 0; i < flightFormationTargetPositions.size(); i++) {

                Vector3 randomRotationAxis = new Vector3(0,1,1);
//                float randomRotationAmount = 3.0f / 60f;
                float rotationAmount = this.rotationAxisRotationRates.get(i);

                Vector3 rotationAxis = this.rotationAxes.get(i);
                Vector3 targetPosition = this.flightFormationTargetPositions.get(i).position;

                rotationAxis.rotate(randomRotationAxis, rotationAmount);
                targetPosition.rotate(randomRotationAxis, rotationAmount);
            }
        }

        // integrate rotation
        this.rotation += this.rotationRate * dt;


        for(int i = 0; i < this.flightFormationInitial.size(); i++) {
            Vector3 position = this.flightFormationInitial.get(i).position.cpy();
            Vector3 velocity = this.flightFormationInitial.get(i).velocity.cpy();
            float diameter = this.flightFormationInitial.get(i).diameter;
            String scheme = "all";
            float speedMult = 1;

            // copy ribbon to get its properties, aside from the ones we are going to change
            Ribbon ribbonInitialCopy = new Ribbon(position, velocity, diameter, scheme, speedMult);

            // make copy because flightFormationInitial must not be altered
            ribbonInitialCopy.position = ribbonInitialCopy.position.cpy();

            // rotate
            ribbonInitialCopy.position.rotate(this.rotationAxes.get(i), this.rotation);

            ribbonInitialCopy.position.add(this.formationCentrePosition.cpy());

            // todo: copy or generate rotation axis

            this.flightFormationTargetPositions.set(i, ribbonInitialCopy);
        }


        // begin steer the whole formation
        randomiseAxisDirection(steeringAxis, 0.0f, TRAJECTORY_TURNING_SPEED_MAX_DELTA * dt * speedSettingFloat,true);
        int direction = rnd.nextBoolean() ? 1 : -1;
        trajectoryTurningSpeed += TRAJECTORY_TURNING_SPEED_MAX_DELTA * rnd.nextFloat() * direction * speedSettingFloat;
        formationCentreVelocity.rotate(steeringAxis, trajectoryTurningSpeed * dt);
        // end steer the whole formation




        // if last time we looked not all were locked, see if all are locked now
        if(allLocked == false) {
            boolean locked = true;
            for (int i = 0; i < this.locked.length; i++) {
                if(this.locked[i] == false) {
                    locked = false;
                    break;
                }
            }
            if(locked == true) {
                allLocked = true;
            }
        }

        if(
                (
                        (formationCentrePosition.x > BOUNDARY_RIGHT ) ||
                                (formationCentrePosition.x < BOUNDARY_LEFT  ) ||
                                (formationCentrePosition.y > BOUNDARY_BOTTOM) ||
                                (formationCentrePosition.y < BOUNDARY_TOP   ) ||
                                (formationCentrePosition.z > BOUNDARY_BACK  ) ||
                                (formationCentrePosition.z < BOUNDARY_FRONT )
                )
        ) {
            centreAccelerateHome(dt);
        }
        approachSpeedLimit();
    }


}
