package com.beachcafesoftware.ribbons;

import java.util.Comparator;

/**
 * Created by Guy Aaltonen on 27/01/19.
 */

public class DustCloudPuffComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {

        DustCloudPuff p1 = (DustCloudPuff)o1;
        DustCloudPuff p2 = (DustCloudPuff)o2;

        if(p1.position.z > p2.position.z) {
            return -1;
        }
        else if(p1.position.z < p2.position.z) {
            return 1;
        }
        else return 0;
    }

}
