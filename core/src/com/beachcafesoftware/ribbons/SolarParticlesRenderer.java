package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 5/01/19.
 */

// TODO: THIS IS FUCKING AWESOME! IT'S LIKE SOLAR WINDS! BUT IT NEEDS EACH FORMATION TO ASSIGN A GOOD VALUE TO VELOCITY EACH FRAME!!!
// TODO: THIS IS FUCKING AWESOME! IT'S LIKE SOLAR WINDS! BUT IT NEEDS EACH FORMATION TO ASSIGN A GOOD VALUE TO VELOCITY EACH FRAME!!!
// TODO: THIS IS FUCKING AWESOME! IT'S LIKE SOLAR WINDS! BUT IT NEEDS EACH FORMATION TO ASSIGN A GOOD VALUE TO VELOCITY EACH FRAME!!!
// TODO: THIS IS FUCKING AWESOME! IT'S LIKE SOLAR WINDS! BUT IT NEEDS EACH FORMATION TO ASSIGN A GOOD VALUE TO VELOCITY EACH FRAME!!!
// TODO: THIS IS FUCKING AWESOME! IT'S LIKE SOLAR WINDS! BUT IT NEEDS EACH FORMATION TO ASSIGN A GOOD VALUE TO VELOCITY EACH FRAME!!!
// TODO: THIS IS FUCKING AWESOME! IT'S LIKE SOLAR WINDS! BUT IT NEEDS EACH FORMATION TO ASSIGN A GOOD VALUE TO VELOCITY EACH FRAME!!!
// TODO: THIS IS FUCKING AWESOME! IT'S LIKE SOLAR WINDS! BUT IT NEEDS EACH FORMATION TO ASSIGN A GOOD VALUE TO VELOCITY EACH FRAME!!!
// TODO: THIS IS FUCKING AWESOME! IT'S LIKE SOLAR WINDS! BUT IT NEEDS EACH FORMATION TO ASSIGN A GOOD VALUE TO VELOCITY EACH FRAME!!!
// TODO: THIS IS FUCKING AWESOME! IT'S LIKE SOLAR WINDS! BUT IT NEEDS EACH FORMATION TO ASSIGN A GOOD VALUE TO VELOCITY EACH FRAME!!!
// TODO: THIS IS FUCKING AWESOME! IT'S LIKE SOLAR WINDS! BUT IT NEEDS EACH FORMATION TO ASSIGN A GOOD VALUE TO VELOCITY EACH FRAME!!!

// TODO: touch up logic to render a ribbon even if it's not active anymore. as of now, when a ribbon is replaced,
// TODO: the whole thing disappears at once. it would be nice for it to shrink and then disappear.
// TODO: ribbon only gets replaced every 90 seconds, so it's not urgent, but it would look better.
public class SolarParticlesRenderer extends FrameBufferBasedRenderer implements Renderer {

    public static final float SPRITE_LIFE = 9.0f;

    private Particle[] particles = new Particle[1000000];
    private int startIndex = 0;
    private int endIndex = 0;

    private final int UPS;
    private final int targetFps;
    float color;
    SpriteBatch sb;
    Texture star;
    static float[] points = new float[20];
    private float RIBBON_DIAMETER;
    private Random rnd = new Random();


    @Override
    public void drawAll(int width, int height, int currentFrame, ArrayList<Formation> activeFormations) {

        // reset array if near capacity
        if(endIndex > 900000) {
            Particle[] newParticles = new Particle[1000000];

            int c = 0;
            for(int i = startIndex; i < endIndex; i++) {
                newParticles[c] = particles[i];
                c++;
            }
            particles = newParticles;

            endIndex = endIndex - startIndex;
            startIndex = 0;
        }

        for (int k = 0; k * targetFps < UPS; k++) {
            float delta = (1f / UPS);

            for (int i = 0; i < activeFormations.size(); i++) {
                activeFormations.get(i).updateAllRibbons(delta);

                ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

                for (int j = 0; j < formationRibbons.size(); j++) {
                    putParticles(formationRibbons.get(j));
                }
            }
        }

        drawParticles(currentFrame, width, height);
    }

    private void drawParticles(int currentFrame, int width, int height) {

        if (currentFrame % targetFps * 4 == 0) {
            sb.dispose();
            sb = new SpriteBatch();
            sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        }

        // draw ribbons onto framebuffer
        fb.begin();

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sb.begin();

        // move start index to first alive particle
        int c = startIndex;

        if(targetFps == 30 || (currentFrame % 2 == 0)) {
            while(c < endIndex) {
                Particle particle = particles[c];
                particle.life -= 0.1;

                if (particle.life < 0) {
                    startIndex = c;
                } else {
                    //particle.position.add(particle.velocity.cpy().scl(0.1f));
                    particle.position.add(particle.velocity);
                }
                c++;
            }
        }

        // draw particles
        for(int i = startIndex; i < endIndex; i++) {
            Particle particle = particles[i];

            if(particle.position.z < 0) {
                continue;
            }   // near z clipping

            float reciprocalOfZ = 1f / particle.position.z;
            int shortSideLength = Math.min(width,height);

            float colori = particle.color;
            float dxh = particle.position.x;
            float dyh = particle.position.y;
            float xc = (width * 0.5f)  + ((dxh) * reciprocalOfZ) * shortSideLength;
            float yc = (height * 0.5f) + ((dyh) * reciprocalOfZ) * shortSideLength;

            float diameterSmall = ((RIBBON_DIAMETER * shortSideLength) * reciprocalOfZ) * particle.life;
            float x1 = xc - diameterSmall * 0.5f;
            float y1 = yc - diameterSmall * 0.5f;
            float x2 = x1 + diameterSmall;
            float y2 = y1 + diameterSmall;

            points[0] = x1 ;
            points[1] = y1 ;
            points[2] = colori;
            points[5] = x2 ;
            points[6] = y1 ;
            points[7] = colori;
            points[10] = x2 ;
            points[11] = y2 ;
            points[12] = colori;
            points[15] = x1 ;
            points[16] = y2 ;
            points[17] = colori;

            sb.draw(star, points, 0, 20);
        }

        sb.end();
        fb.end();

        // draw framebuffer onto screen
        sb.begin();
        sb.draw(fb.getColorBufferTexture(), 0f, 0f, width, height);
        sb.end();

    }

    public void putParticles(Ribbon ribbon) {
        putParticle(ribbon);
    }


    public void putParticle(Ribbon ribbon) {

        if(ribbon.position.z < -200) { return; }   // near z clipping

        float alphaFactor = 0.95f;

         double lum = 0.50f + rnd.nextFloat() * 0.49f;

        float colori = com.badlogic.gdx.graphics.Color.toFloatBits(
                (float)(ribbon.r * 2000 * lum),
                (float)(ribbon.g * 2000 * lum),
                (float)(ribbon.b * 2000 * lum),
                alphaFactor
        );

        Particle particle = new Particle();
        particle.position = ribbon.position.cpy();
        particle.life = SPRITE_LIFE;

        // this is more like classic solar winds, because it has no random velocity component
       // particle.velocity = ribbon.velocity.cpy().scl(0.075f);

        // this is less like classic solar winds, but the random velocity component is cool too. maybe make another renderer?
        particle.velocity = new Vector3().setToRandomDirection().scl(ribbon.speed * 0.01f).add(ribbon.velocity.cpy().scl(0.075f)).scl(0.1f);

        particle.color = colori;

        particles[endIndex] = particle;
        endIndex++;
    }

    @Override
    public void generateBlanker(float speedSetting) {

    }

    @Override
    public void blankOut(SpriteBatch sb, int width, int height) {

    }

    @Override
    public void removeRibbon(Ribbon r) {
        // TODO: because we keep references to ribbons in this renderer, remove our own reference.
        // TODO: however, we should still putCloudPuffs the "expired" ribbon, and reduce its size,
        // TODO: until there is none left. it looks better than removing the whole thing at once.
        // TODO: this is a temporary hack.
        // TODO: probably move the ribbon into a temporary list for expired ribbons.
        // TODO: and of course render those, and reduce their size each frame.
        // TODO: ribbons get replaced every 90 seconds so it's not a huge deal.
        // TODO: but it would still be preferable.
    }

    public SolarParticlesRenderer(int targetFps, int UPS, String imageQuality, String filtering, int width, int height, int diameter) {
        String prefix = "";
        String filepath = new String(prefix + "glow" + ".png");
        FileHandle handle = Gdx.files.internal(filepath);
        star = new Texture(handle, Pixmap.Format.RGBA8888, false);

        this.targetFps = targetFps;
        this.UPS = UPS;

        this.initialiseFrameBuffer(imageQuality, width, height);

        this.sb = new SpriteBatch();
        sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);

        Texture.TextureFilter filter = filtering.equals("nearest") ? Texture.TextureFilter.Nearest : Texture.TextureFilter.Linear;
        fb.getColorBufferTexture().setFilter(filter,filter);

        RIBBON_DIAMETER = diameter / 16.0f;

        Gdx.gl20.glDisable(GL20.GL_DEPTH_TEST);

        Gdx.gl20.glEnable(GL20.GL_BLEND);
        Gdx.gl20.glBlendEquation(GL20.GL_FUNC_ADD);

        points[3] = 0;
        points[4] = 0;
        points[8] = 1;
        points[9] = 0;
        points[13] = 1;
        points[14] = 1;
        points[18] = 0;
        points[19] = 1;

    }

}
