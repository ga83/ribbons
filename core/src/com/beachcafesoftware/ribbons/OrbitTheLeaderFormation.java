package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 1/11/17.
 */
// TODO: the rotation axis of ribbon is not being updated when accelerating towards the leader.
// TODO: this makes the rendering of ribbons via ShardsRenderer strictly speaking wrong,
// TODO: although it is not easy to notice.
public class OrbitTheLeaderFormation implements Formation {

    private static int MINIMUM_FORMATIONS = 1;
    private static int MAXIMUM_FORMATIONS = 3;

    public static float MIN_SPEED = 150.0f;
    private static float MAX_DELTA_ROTATE = 7.0f;     // degrees per second. set higher for fun.
    private static float ACCEL_HOME_FRACTION = 0.066f;
    private static float ACCEL_TO_LEADER_FRACTION = 0.15f;

    static float BOUNDARY_RIGHT =  1.0f + 450.0f;
    static float BOUNDARY_LEFT =   0.0f - 450.0f;
    static float BOUNDARY_BOTTOM = 1.0f + 450.0f;
    static float BOUNDARY_TOP =    0.0f - 450.0f;
    static float BOUNDARY_FRONT =  500.0f;
    static float BOUNDARY_BACK =   700.0f;

    ArrayList<Ribbon> ribbons;

    Vector3 leaderPosition;
    Vector3 leaderVelocity;
    Vector3 rotationAxis;
    float leaderSpeed;
    float turningAngle;
    boolean returningHome;
    Random rnd;
    private int lastReplacedIndex;
    private float speedSettingFloat;


    public OrbitTheLeaderFormation() {

        System.out.println("aaa OTLF");

        rnd = new Random();
        leaderPosition = new Vector3(-300 + rnd.nextFloat() * 600.0f, -300 + rnd.nextFloat() * 600.0f,1000);
        leaderVelocity = new Vector3(0,0,MIN_SPEED);
        rotationAxis = new Vector3(0,0,1);
        this.leaderSpeed = MIN_SPEED;
        rnd = new Random();
        returningHome = false;
        turningAngle = 0;
        this.ribbons = new ArrayList<Ribbon>();
        this.lastReplacedIndex = 0;
    }



    @Override
    public void removeAllRibbons() {
        this.ribbons.clear();
    }

    @Override
    public void addRibbons(ArrayList<Ribbon> ribbons, float speedMultiplier) {
        for(int i = 0; i < ribbons.size(); i++) {
            ribbons.get(i).setRandomSpeed(speedMultiplier * 0.75f, false);
        }

        this.ribbons.addAll(ribbons);
    }

    @Override
    public void setSpeed(float speed) {
        this.speedSettingFloat = speed;
        leaderVelocity.setToRandomDirection();
        leaderSpeed *= speed;
        leaderVelocity.nor().scl(leaderSpeed);
    }

    @Override
    public void updateAllRibbons(float dt) {
        updateLeader(dt);
        for(int i = 0; i < ribbons.size(); i++) {
            Ribbon r = ribbons.get(i);
            updateIndividualRibbon((float)dt,r);
        }
    }

    @Override
    public ArrayList<Ribbon> getRibbons() {
        return ribbons;
    }

    @Override
    public int getNextReplacedIndex() {
        lastReplacedIndex ++;
        if(lastReplacedIndex >= ribbons.size()) {
            lastReplacedIndex = 0;
        }

        return lastReplacedIndex;
    }

    @Override
    public int getMinimumFormations() {
        return MINIMUM_FORMATIONS;
    }

    @Override
    public int getMaximumFormations() {
        return MAXIMUM_FORMATIONS;
    }

    private void updateLeader(float dt) {
        leaderPosition.x += leaderVelocity.x * dt;
        leaderPosition.y += leaderVelocity.y * dt;
        leaderPosition.z += leaderVelocity.z * dt;

        randomiseRotationAxis(rotationAxis, 0.0f * dt, 90.0f * dt);
        int direction;      // just negative or positive
        direction = rnd.nextBoolean() ? 1 : -1;
        turningAngle += MAX_DELTA_ROTATE * dt * rnd.nextFloat() * direction * (leaderSpeed / 450f);

        if(returningHome == false) {
            leaderVelocity.rotate(rotationAxis, turningAngle);
        }

        // ease out to avoid tight loops
        turningAngle = turningAngle * (1 - (dt * 0.1f));

        if(
                (leaderPosition.x > BOUNDARY_RIGHT ) ||
                        (leaderPosition.x < BOUNDARY_LEFT  ) ||
                        (leaderPosition.y > BOUNDARY_BOTTOM) ||
                        (leaderPosition.y < BOUNDARY_TOP   ) ||
                        (leaderPosition.z > BOUNDARY_BACK  ) ||
                        (leaderPosition.z < BOUNDARY_FRONT )
                )    {
            leaderAccelerateHome();
        }
        approachSpeedLimit();
    }

    private void randomiseRotationAxis(Vector3 rotationAxis, float minAngle, float maxAngle) {
        int axis = rnd.nextInt(3);
        Vector3 rotationAxis2;

        if(axis == 0)      { rotationAxis2 = new Vector3(1,0,0); }
        else if(axis == 1) { rotationAxis2 = new Vector3(0,1,0); }
        else               { rotationAxis2 = new Vector3(0,0,1); }

        int sign = rnd.nextBoolean() ? 1 : -1;
        float range = maxAngle - minAngle;
        float angle = minAngle + rnd.nextFloat() * range * sign;

        rotationAxis.rotate(rotationAxis2,angle);
    }

    private void approachSpeedLimit() {
        if(leaderVelocity.len() > leaderSpeed) {
//            leaderVelocity.scl(0.99f);
            leaderVelocity.scl(0.9f);
        }
        else if(leaderVelocity.len() < leaderSpeed) {
//            leaderVelocity.scl(1.0f / 0.99f);
            leaderVelocity.scl(1.0f / 0.9f);
        }
    }

    /*
    private void leaderAccelerateHome(Ribbon ribbon) {
        Vector3 pointingHomeNormalised = (new Vector3(0 - leaderPosition.x, 0 - leaderPosition.y, 0 - leaderPosition.z)).nor();
        ribbon.velocity.add(pointingHomeNormalised.scl(leaderSpeed * ACCEL_HOME_FRACTION));
    }
*/

    private void leaderAccelerateHome() {
        Vector3 pointingHomeNormalised = (new Vector3(0 - leaderPosition.x, 0 - leaderPosition.y, BOUNDARY_FRONT - leaderPosition.z)).nor();
        leaderVelocity.add(pointingHomeNormalised.scl(leaderSpeed * ACCEL_HOME_FRACTION));
    }

    private void ribbonAccelerateToLeader(Ribbon ribbon) {
        Vector3 pointingLeaderNormalised = (new Vector3(leaderPosition.x - ribbon.position.x,leaderPosition.y - ribbon.position.y,leaderPosition.z - ribbon.position.z)).nor();
        ribbon.velocity.add(pointingLeaderNormalised.scl(ribbon.speed * ACCEL_TO_LEADER_FRACTION * speedSettingFloat));
    }


    public void updateIndividualRibbon(float dt,Ribbon ribbon) {
        ribbon.position.x += ribbon.velocity.x * dt;
        ribbon.position.y += ribbon.velocity.y * dt;
        ribbon.position.z += ribbon.velocity.z * dt;

        ribbon.randomiseRotationAxis(ribbon.rotationAxis, 0.0f * dt, 90.0f * dt);
        int direction;      // just negative or positive
        direction = ribbon.rnd.nextBoolean() ? 1 : -1;
        ribbon.turningAngle += Ribbon.MAX_DELTA_ROTATE * dt * ribbon.rnd.nextFloat() * direction * (ribbon.speed / 450f);

        if(ribbon.returningHome == false) {
            ribbon.velocity.rotate(ribbon.rotationAxis, ribbon.turningAngle);
        }

        // ease out to avoid tight loops
        ribbon.turningAngle = ribbon.turningAngle * (1 - (dt * 0.1f));

        /*
        if(
                (ribbon.position.x > Ribbon.BOUNDARY_RIGHT ) ||
                        (ribbon.position.x < Ribbon.BOUNDARY_LEFT  ) ||
                        (ribbon.position.y > Ribbon.BOUNDARY_BOTTOM) ||
                        (ribbon.position.y < Ribbon.BOUNDARY_TOP   ) ||
                        (ribbon.position.z > Ribbon.BOUNDARY_BACK  ) ||
                        (ribbon.position.z < Ribbon.BOUNDARY_FRONT )
                )    {
            leaderAccelerateHome(ribbon);
        }
        */

        ribbonAccelerateToLeader(ribbon);
        ribbon.approachSpeedLimit();
    }


}
