package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;


/**
 * Created by Guy Aaltonen on 5/01/19.
 */
public class GlowingSmokeRenderer extends FrameBufferBasedRenderer implements Renderer {

    private static final float MAX_PUFF_LIFE = 6.0f;
    private ArrayList<DustCloudPuff> puffs = new ArrayList<DustCloudPuff>();
    private final int UPS;
    private final int targetFps;
    static Texture glow;
    static float[] points = new float[20];
    SpriteBatch sb;
    Random rnd = new Random();
    private int RIBBON_DIAMETER;


    @Override
    public void drawAll(int width, int height, int currentFrame, ArrayList<Formation> activeFormations) {

        for (int k = 0; k * targetFps < UPS; k++) {

            float delta = (1f / UPS);

            for (int i = 0; i < activeFormations.size(); i++) {
                activeFormations.get(i).updateAllRibbons(delta);

                ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

                for (int j = 0; j < formationRibbons.size(); j++) {
                    putCloudPuffs(formationRibbons.get(j));
                }
             }
        }

        drawCloudPuffs(currentFrame, width, height);
    }

    private void drawCloudPuffs(int currentFrame, int width, int height) {

        if (currentFrame % targetFps * 4 == 0) {
            sb.dispose();
            sb = new SpriteBatch();
        }

        // draw ribbons onto framebuffer
        fb.begin();

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sb.begin();

        sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);

        // remove dead puffs
        int c = 0;

        if(targetFps == 30 || (currentFrame % 2 == 0)) {
            while (c < puffs.size()) {

                DustCloudPuff puff = puffs.get(c);
                puff.life -= 0.1;

                if (puff.life < 0) {
                    puffs.remove(c);
                } else {
                    c++;
                }
            }
        }

        for(int i = 0; i < puffs.size(); i++) {

            DustCloudPuff puff = puffs.get(i);

            float alphaFactor = puff.life * puff.life * puff.life * 0.0065f;
            alphaFactor =  Math.min(Math.max(0.0f, alphaFactor), 1.0f);

            float colori = com.badlogic.gdx.graphics.Color.toFloatBits(
                    puff.r * 2000 * 2, puff.g * 2000 * 2, puff.b * 2000 * 2, alphaFactor
            );

            float reciprocalOfZ = 1f / puff.position.z;
            int shortSideLength = Math.min(width,height);

            float diameterSmall =
                    ((RIBBON_DIAMETER * shortSideLength) * reciprocalOfZ) * 0.25f +
                            ((RIBBON_DIAMETER * shortSideLength) * reciprocalOfZ * 0.33f * (MAX_PUFF_LIFE - puff.life));

            float dxh = puff.position.x;
            float dyh = puff.position.y;
            float xc = (width * 0.5f)  + ((dxh) * reciprocalOfZ) * shortSideLength;
            float yc = (height * 0.5f) + ((dyh) * reciprocalOfZ) * shortSideLength;

            float x1 = xc - diameterSmall * 0.5f;
            float y1 = yc - diameterSmall * 0.5f;
            float x2 = x1 + diameterSmall;
            float y2 = y1 + diameterSmall;

            points[0] = x1 ;
            points[1] = y1 ;
            points[2] = colori;
            points[5] = x2 ;
            points[6] = y1 ;
            points[7] = colori;
            points[10] = x2 ;
            points[11] = y2 ;
            points[12] = colori;
            points[15] = x1 ;
            points[16] = y2 ;
            points[17] = colori;

            sb.draw(glow, points, 0, 20);
        }

        sb.end();
        fb.end();

        // draw framebuffer onto screen
        sb.begin();
        sb.draw(fb.getColorBufferTexture(), 0f, 0f, width, height);
        sb.end();

    }


    public void putCloudPuffs(Ribbon ribbon) {
        putCloudPuff(ribbon);
    }


    public void putCloudPuff(Ribbon ribbon) {
        if(ribbon.position.z <= 0) { return; }   // near z clipping

        Vector3 randomisedPosition = ribbon.position.cpy();

        DustCloudPuff puff = new DustCloudPuff();
        puff.position = randomisedPosition;

        puff.life = MAX_PUFF_LIFE;

        puff.r = ribbon.r;
        puff.g = ribbon.g;
        puff.b = ribbon.b;

        puffs.add(puff);
    }


    @Override
    public void generateBlanker(float speedSetting) {

    }

    @Override
    public void blankOut(SpriteBatch sb, int width, int height) {

    }

    @Override
    public void removeRibbon(Ribbon r) {
        // i don't think we need to hold references to ribbons in this class,
        // but if so, remember to release them here
    }

    public GlowingSmokeRenderer(int targetFps, int UPS, String imageQuality, String filtering, int width, int height, int diameter) {

        String prefix = "";
        String filepath = new String(prefix + "glow" + ".png");
        FileHandle handle = Gdx.files.internal(filepath);
        glow = new Texture(handle, Pixmap.Format.RGBA8888, false);

        this.targetFps = targetFps;
        this.UPS = UPS;

        this.initialiseFrameBuffer(imageQuality, width, height);

        this.sb = new SpriteBatch();

        Texture.TextureFilter filter = filtering.equals("nearest") ? Texture.TextureFilter.Nearest : Texture.TextureFilter.Linear;
        fb.getColorBufferTexture().setFilter(filter,filter);

//        RIBBON_DIAMETER = (int) (diameter * 0.75);
        RIBBON_DIAMETER = (int) (diameter * 1.5);

        Gdx.gl20.glDisable(GL20.GL_DEPTH_TEST);

        points[3] = 0;
        points[4] = 0;
        points[8] = 1;
        points[9] = 0;
        points[13] = 1;
        points[14] = 1;
        points[18] = 0;
        points[19] = 1;

    }

}

