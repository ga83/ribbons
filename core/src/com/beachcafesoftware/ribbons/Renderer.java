package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.ArrayList;

/**
 * Created by Guy Aaltonen on 23/01/18.
 */

public interface Renderer {
    void drawAll(int width, int height, int currentFrame, ArrayList<Formation> activeFormations);
    void generateBlanker(float speedSetting);
    void blankOut(SpriteBatch sb,int width, int height);
    void removeRibbon(Ribbon r);
}
