package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 3/11/17.
 */


// TODO: the rotation axis probably isn't set each frame. it needs to be correct, because the ShardsRenderer relies on the rotation axis to render properly.
// TODO: however, calculating rotationaxis every frame will be wasting cpu when using renderer that doen't need it.
// TODO: it seems crazy, but i think we should make copies of all formations, one with that calculation, and one without.
public class SpinningStickFormation implements Formation {

    private static float APPROACH_SPEED = 400;
    private float approachSpeed = APPROACH_SPEED;

    private static int MINIMUM_FORMATIONS = 1;
    private static int MAXIMUM_FORMATIONS = 3;

//    public static float MIN_STICK_HALF_LENGTH = 500.0f;     // length to centre, ie radius
//    public static float MAX_STICK_HALF_LENGTH = 710.0f;     // length to centre, ie radius

    public static float MIN_GAP_BETWEEN_RIBBONS = 15;
    public static float MAX_GAP_BETWEEN_RIBBONS = 45;

    public static float MAX_SPEED = 900.0f;
    public static float MIN_SPEED = 200.0f;

    private float trajectoryTurningSpeed = 0.0f;                               // degrees per second
    private static float TRAJECTORY_TURNING_SPEED_MAX_DELTA = 0.5f;     // degrees per second per second

    private float spinningSpeed = 0.0f;                                  // degrees per second
    private static float SPINNING_SPEED_MAX_DELTA = 80.0f;               // degrees per second per second
    private static float SPINNING_SPEED_TARGET = 180.0f;               // degrees per second per second

    private float spinningAxisSpeed = 10.0f;                         // degrees per second
    private static float SPINNING_AXIS_SPEED_MIN_DELTA = 0.0f;          // degrees per second per second
    private static float SPINNING_AXIS_SPEED_MAX_DELTA = 0.0f;          // degrees per second per second

    private static float ACCEL_HOME_FRACTION = 0.022f;

    static float BOUNDARY_RIGHT =  1.0f + 450.0f;
    static float BOUNDARY_LEFT =   0.0f - 450.0f;
    static float BOUNDARY_BOTTOM = 1.0f + 450.0f;
    static float BOUNDARY_TOP =    0.0f - 450.0f;
    static float BOUNDARY_FRONT =  300.0f;
    static float BOUNDARY_BACK =   700.0f;

    ArrayList<Ribbon> ribbons;

    Vector3 centrePosition;     // absolute position of centre, as in normal ribbon's position
    Vector3 centreVelocity;     // absolute velocity of centre, as in normal ribbon's velocity
    Vector3 frontPosition;      // relative to centre. "back position" is calculated on the fly, and is the same distance from the centre as the front.

    Vector3 trajectoryAxis;     // the axis around which the centre steers, as in normal ribbon's rotationAxis
    Vector3 spinningAxis;       // the axis around which the stick is spinning
    Vector3 spinningAxisAxis;   // the axis around which the spinningAxis rotates, for more fluid changes

    float leaderSpeed;
    boolean returningHome;
    Random rnd = new Random();
    private int lastReplacedIndex;
    private float speedSettingFloat;

    private boolean[] locked;
    private boolean allLocked;


    public SpinningStickFormation() {
        centrePosition = new Vector3(
                -300 + rnd.nextFloat() * 600,
                -300 + rnd.nextFloat() * 600,
                200 + rnd.nextFloat() * 500
        );
        centreVelocity = new Vector3().setToRandomDirection().scl(MIN_SPEED + rnd.nextFloat() * (MAX_SPEED - MIN_SPEED));
        trajectoryAxis = new Vector3().setToRandomDirection();

        frontPosition = new Vector3(0,0,1);
        spinningAxis = new Vector3(0,1,0);
        spinningAxisAxis = new Vector3(1,0,0);

        leaderSpeed = MIN_SPEED;
        rnd = new Random();
        returningHome = false;
        trajectoryTurningSpeed = 0;
        ribbons = new ArrayList<Ribbon>();
        lastReplacedIndex = 0;
        allLocked = false;
    }

    @Override
    public void setSpeed(float speed) {
        this.speedSettingFloat = speed;
        this.spinningSpeed *= speedSettingFloat;
        this.spinningAxisSpeed *= speedSettingFloat;
        this.centreVelocity.scl(speedSettingFloat);
    }

    @Override
    public void removeAllRibbons() {
        this.ribbons.clear();
    }

    @Override
    public void addRibbons(ArrayList<Ribbon> ribbons,float speedMult) {
        // do something with the speedMult because it should apply before the ribbons are locked in place
        this.ribbons.addAll(ribbons);
        locked = new boolean[ribbons.size()];

        float gap = MIN_GAP_BETWEEN_RIBBONS + rnd.nextFloat() * (MAX_GAP_BETWEEN_RIBBONS - MIN_GAP_BETWEEN_RIBBONS);
        float stickLength = gap * ribbons.size();
        this.frontPosition = new Vector3(stickLength, 0, 0);
    }

    @Override
    public void updateAllRibbons(float dt) {
        updateLeader(dt);

        float numRibbonsOnStick = ribbons.size();
        Vector3 end1 = centrePosition.cpy().add(frontPosition);
        Vector3 end2 = centrePosition.cpy().sub(frontPosition);
        Vector3 stick = end2.cpy().sub(end1);   // contains the vector which represents the stick itself, ie the length and direction of it

        // set the target position of every ribbon, and then move them towards it, or just move it directly if locked already
        for(int i = 0; i < ribbons.size(); i++) {
            Ribbon r = ribbons.get(i);

            Vector3 oldPosition = r.position.cpy();
            Vector3 ribbonTargetPosition = end1.cpy().add( stick.cpy().scl(1 * ((float)i / (numRibbonsOnStick - 1))) );
            Vector3 towardsTarget = ribbonTargetPosition.cpy().sub(r.position);

            if(locked[i] == true) {
                r.position.x = ribbonTargetPosition.x;
                r.position.y = ribbonTargetPosition.y;
                r.position.z = ribbonTargetPosition.z;
            }
            else {
                // steer towards target
                Vector3 towardsTargetVelocity = towardsTarget.cpy().nor().scl(approachSpeed * speedSettingFloat);
                if(towardsTargetVelocity.len() * dt > towardsTarget.len()) {
                    locked[i] = true;
                }
                else {
                    r.velocity.add(towardsTargetVelocity);
                    r.velocity.nor().scl(approachSpeed * speedSettingFloat);
                    r.updatePosition(dt);
                }
            }

            // for color calculation
            float distanceTravelled = oldPosition.cpy().dst(r.position);
            float ribbonSpeed = distanceTravelled / dt;

            r.speed = ribbonSpeed;
            r.velocity.nor().scl(ribbonSpeed);

            approachSpeed *= 1.000001f;
        }

    }

    private void updateLeader(float dt) {
        centrePosition.x += centreVelocity.x * dt;
        centrePosition.y += centreVelocity.y * dt;
        centrePosition.z += centreVelocity.z * dt;

        randomiseAxisDirection(trajectoryAxis, 0.0f, TRAJECTORY_TURNING_SPEED_MAX_DELTA * dt * speedSettingFloat,true);
        randomiseAxisDirection(spinningAxisAxis, SPINNING_AXIS_SPEED_MIN_DELTA * dt * speedSettingFloat, SPINNING_AXIS_SPEED_MAX_DELTA * dt * speedSettingFloat,false);

        int direction;      // just negative or positive
        direction = rnd.nextBoolean() ? 1 : -1;
        trajectoryTurningSpeed += TRAJECTORY_TURNING_SPEED_MAX_DELTA * rnd.nextFloat() * direction * speedSettingFloat;

        // THIS COMPARISON SEEMS UNNECESSARY BECAUSE RETURNINGHOME IS NEVER TRUE ANYWAY
        if(returningHome == false) {
            // rotate the direction of travel, ie steer its trajectory
            centreVelocity.rotate(trajectoryAxis, trajectoryTurningSpeed * dt);
        }

        // rotate the rotation axis
        spinningAxisSpeed += SPINNING_AXIS_SPEED_MAX_DELTA * rnd.nextFloat() * direction; // * speedSettingFloat;
        spinningAxis.rotate(spinningAxisAxis, spinningAxisSpeed * speedSettingFloat * dt);

        // if last time we looked not all were locked, see if all are locked now
        if(allLocked == false) {
            boolean locked = true;
            for (int i = 0; i < this.locked.length; i++) {
                if(this.locked[i] == false) {
                    locked = false;
                    break;
                }
            }
            if(locked == true) {
                allLocked = true;
//                System.out.println("all locked!");
            }
        }

        // rotate the stick itself
//        spinningSpeed += SPINNING_SPEED_MAX_DELTA * rnd.nextFloat() * direction; // * speedSettingFloat;
        if(allLocked == true) {
            spinningSpeed += SPINNING_SPEED_MAX_DELTA * dt * rnd.nextFloat(); // * speedSettingFloat;
        }
        
        if(spinningSpeed > SPINNING_SPEED_TARGET) {
            spinningSpeed = SPINNING_SPEED_TARGET;
        }

        frontPosition.rotate(spinningAxis, spinningSpeed * speedSettingFloat * dt);

        // hmm think we have to rotate the stick as well when we do this
        frontPosition.rotate(spinningAxisAxis, spinningAxisSpeed * dt);

        // ease out to avoid tight loops
        trajectoryTurningSpeed = trajectoryTurningSpeed * (1 - (0.1f));

        if(
                        (centrePosition.x > BOUNDARY_RIGHT ) |
                        (centrePosition.x < BOUNDARY_LEFT  ) |
                        (centrePosition.y > BOUNDARY_BOTTOM) |
                        (centrePosition.y < BOUNDARY_TOP   ) |
                        (centrePosition.z > BOUNDARY_BACK  ) |
                        (centrePosition.z < BOUNDARY_FRONT )
        )    {
            centreAccelerateHome();
        }
        approachSpeedLimit();
    }

    public void randomiseAxisDirection(Vector3 rotationAxis, float minAngle, float maxAngle, boolean negativePossible) {
        int axis = rnd.nextInt(3);
        Vector3 rotationAxis2;

        if(axis == 0)      { rotationAxis2 = new Vector3(1,0,0); }
        else if(axis == 1) { rotationAxis2 = new Vector3(0,1,0); }
        else               { rotationAxis2 = new Vector3(0,0,1); }

        int sign;
        if(negativePossible == true) {
            sign = rnd.nextBoolean() ? 1 : -1;
        }
        else {
            sign = 1;
        }
        float range = maxAngle - minAngle;
        float angle = minAngle + rnd.nextFloat() * range * sign;

        rotationAxis.rotate(rotationAxis2,angle);
    }

    private void centreAccelerateHome() {
        Vector3 pointingHomeNormalised = (new Vector3(0 - centrePosition.x, 0 - centrePosition.y, BOUNDARY_FRONT - centrePosition.z)).nor();
        centreVelocity.add(pointingHomeNormalised.scl(leaderSpeed * ACCEL_HOME_FRACTION));
    }

    @Override
    public ArrayList<Ribbon> getRibbons() {
        return ribbons;
    }

    @Override
    public int getNextReplacedIndex() {
        lastReplacedIndex ++;
        if(lastReplacedIndex >= ribbons.size()) {
            lastReplacedIndex = 0;
        }
        return lastReplacedIndex;
    }

    @Override
    public int getMinimumFormations() {
        return MINIMUM_FORMATIONS;
    }

    @Override
    public int getMaximumFormations() {
        return MAXIMUM_FORMATIONS;
    }

    public void approachSpeedLimit() {
        if(centreVelocity.len() > leaderSpeed) {
            centreVelocity.scl(0.99f);
        }
        else if(centreVelocity.len() < leaderSpeed) {
            centreVelocity.scl(1.0f / 0.99f);
        }
    }

}
