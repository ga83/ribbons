package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;

/**
 * author: Guy Aaltonen
 */
// TODO: the rotation axis probably isn't set each frame. it needs to be correct, because the ShardsRenderer relies on the rotation axis to render properly.
public class FireworksFormation implements Formation {

    static float TARGET_BOUNDARY_FRONT =  300.0f;
    static float TARGET_BOUNDARY_BACK =   350.0f;

    static float LEADER_BOUNDARY_RIGHT =  1.0f + 100.0f;
    static float LEADER_BOUNDARY_LEFT =   0.0f - 100.0f;
    static float LEADER_BOUNDARY_BOTTOM = 1.0f + 100.0f;
    static float LEADER_BOUNDARY_TOP =    0.0f - 100.0f;
    static float LEADER_BOUNDARY_FRONT =  -100.0f;
    static float LEADER_BOUNDARY_BACK =   -50.0f;

	public static float RIBBON_MAX_SPEED = 2000.0f;
	public static float RIBBON_MIN_SPEED = 1000.0f;

    public static float LEADER_MAX_SPEED = 200.0f;
    public static float LEADER_MIN_SPEED = 100.0f;

    private static float ACCEL_TARGET_FRACTION = 0.0099f;
	private static float EPSILON = 100;
	private static float LIFETIME = 2.0f;

    private ArrayList<Ribbon> ribbons;

    private Vector3 leaderPosition;
    private Vector3 leaderVelocity;
    private float leaderSpeed;
    private Random rnd;
    private int lastReplacedIndex;
    private float speedSettingFloat;

	private Vector3 target;
	private float life;
	private boolean leaderExploded;
    private boolean justSnapped = false;



    public FireworksFormation() {
        rnd = new Random();

		float xm = LEADER_BOUNDARY_RIGHT - LEADER_BOUNDARY_LEFT;
		float ym = LEADER_BOUNDARY_BOTTOM - LEADER_BOUNDARY_TOP;
		float zm = LEADER_BOUNDARY_BACK - LEADER_BOUNDARY_FRONT;

        leaderPosition = new Vector3(
			LEADER_BOUNDARY_LEFT + rnd.nextFloat() * xm,
			LEADER_BOUNDARY_TOP + rnd.nextFloat() * ym,
			LEADER_BOUNDARY_FRONT + rnd.nextFloat() * zm
		);

        this.leaderSpeed = LEADER_MIN_SPEED + rnd.nextFloat() * (LEADER_MAX_SPEED - LEADER_MIN_SPEED);
        this.leaderVelocity = new Vector3(0,0,1).scl(this.leaderSpeed);

        this.ribbons = new ArrayList<Ribbon>();
        this.lastReplacedIndex = 0;
		this.life = LIFETIME;
		this.leaderExploded = false;


    }

    @Override
    public int getMinimumFormations() {
        return 1;
    }

    @Override
    public int getMaximumFormations() {
        return 1;
    }

    @Override
    public void removeAllRibbons() {
        this.ribbons.clear();
    }

    @Override
    public void addRibbons(ArrayList<Ribbon> ribbons, float speedMultiplier) {
        this.ribbons.addAll(ribbons);

        for(int i = 0; i < ribbons.size(); i++) {
            ribbons.get(i).position.set(0,0,-200.0f); //= new Vector3(0,0,-200.0f);
            ribbons.get(i).velocity = new Vector3(0,0,0);
        }
        setRandomTarget();
    }

    private void setRandomTarget() {
        float zm1 = ((TARGET_BOUNDARY_BACK * ribbons.size() / 100f) - TARGET_BOUNDARY_FRONT);
        float z = TARGET_BOUNDARY_FRONT + rnd.nextFloat() * zm1;

        float x1 = 0 - (z * 0.25f);
        float x2 = (z * 0.25f);

        float y1 = 0 - (z * 0.25f);
        float y2 = (z * 0.25f);

        float x = x1 + rnd.nextFloat() * (x2 - x1);
        float y = y1 + rnd.nextFloat() * (y2 - y1);

        target = new Vector3(x, y , z + EPSILON);

    }

	private void scaleRibbonSpeedsToSpeed(float speed) {
        for(int i = 0; i < ribbons.size(); i++) {
			ribbons.get(i).velocity.scl(speed);
		}
	}

	private void randomiseRibbonVelocities() {
        for(int i = 0; i < ribbons.size(); i++) {
			float ribbonSpeed = RIBBON_MIN_SPEED + rnd.nextFloat() * (RIBBON_MAX_SPEED - RIBBON_MIN_SPEED);
			ribbons.get(i).velocity.setToRandomDirection().scl(ribbonSpeed);
		}
	}

    @Override
    public void setSpeed(float speed) {
        this.speedSettingFloat = speed;
        leaderSpeed *= speed;
        leaderVelocity.nor().scl(leaderSpeed);
		life *= 1 / speed;
    }

    @Override
    public void updateAllRibbons(float dt) {
        updateLeader(dt);

        if(ribbons.size() > 0) {
            updateIndividualRibbon(dt, ribbons.get(0), true);
        }

        if(justSnapped) {
            justSnapped = false;
        } else {
            for (int i = 1; i < ribbons.size(); i++) {
                updateIndividualRibbon(dt, ribbons.get(i), false);
            }
        }

	}

	private void reset() {
		leaderExploded = false;
		float xm = LEADER_BOUNDARY_RIGHT - LEADER_BOUNDARY_LEFT;
		float ym = LEADER_BOUNDARY_BOTTOM - LEADER_BOUNDARY_TOP;
		float zm = LEADER_BOUNDARY_BACK - LEADER_BOUNDARY_FRONT;

        leaderPosition = new Vector3(
			LEADER_BOUNDARY_LEFT + rnd.nextFloat() * xm,
			LEADER_BOUNDARY_TOP + rnd.nextFloat() * ym,
			LEADER_BOUNDARY_FRONT + rnd.nextFloat() * zm
		);

        this.leaderSpeed = LEADER_MIN_SPEED + rnd.nextFloat() * (LEADER_MAX_SPEED - LEADER_MIN_SPEED);
        this.leaderVelocity = new Vector3(0,0,1).scl(this.leaderSpeed);

        this.lastReplacedIndex = 0;
		this.life = LIFETIME;

        setRandomTarget();

		setSpeed(speedSettingFloat);

        for(int i = 0; i < ribbons.size(); i++) {
            ribbons.get(i).position.set(0,0,-200.0f);
            ribbons.get(i).velocity = new Vector3(0,0,0);
        }
	}

    @Override
    public ArrayList<Ribbon> getRibbons() {
        return ribbons;
    }

    @Override
    public int getNextReplacedIndex() {
        lastReplacedIndex ++;

        if(lastReplacedIndex >= ribbons.size()) {
            lastReplacedIndex = 0;
        }

        return lastReplacedIndex;
    }

    private void updateLeader(float dt) {

		if(leaderExploded == false) {

			leaderPosition.x += leaderVelocity.x * dt;
			leaderPosition.y += leaderVelocity.y * dt;
			leaderPosition.z += leaderVelocity.z * dt;

			leaderAccelerateToTarget();
			leaderApproachSpeedLimit();

			// TODO: this makes it look better in ribbons / worms ribbons / transparent ribbons style.
            // TODO: this is because, without it, it suddenly draws a ribbon from the starting point to the explosion point
            // TODO: just after the explosion. it looks better when the ribbons snap to leader before explosion.
            // TODO: however, putting this here makes it look worse on all other styles.
            // TODO: therefore, for now, this formation is disabled.
            snapAllToLeader();

			if(leaderPosition.dst(target) < EPSILON * speedSettingFloat) {
				leaderExploded = true;
                randomiseRibbonVelocities();
         		scaleRibbonSpeedsToSpeed(speedSettingFloat);
                snapAllToLeader();
			}
		}
		else {
			life -= dt;
			if(life <= 0) {
				reset();
			}
		}
    }

    private void snapAllToLeader() {
        this.justSnapped = true;

        for(int i = 0; i < ribbons.size(); i++) {
            snapToLeader(ribbons.get(i));
        }
    }

    private void leaderApproachSpeedLimit() {
        if(leaderVelocity.len() > leaderSpeed) {
            leaderVelocity.scl(0.99f);
        }
        else if(leaderVelocity.len() < leaderSpeed) {
            leaderVelocity.scl(1.0f / 0.99f);
        }
    }

    public void updateIndividualRibbon(float dt, Ribbon ribbon, boolean first) {
		if(leaderExploded == false) {
            if(first == true) {
			    snapToLeader(ribbon);
            }
		}
		else {
			ribbon.position.x += ribbon.velocity.x * dt;
			ribbon.position.y += ribbon.velocity.y * dt;
			ribbon.position.z += ribbon.velocity.z * dt;
		}
    }

    private void snapToLeader(Ribbon ribbon) {
		ribbon.position.x = leaderPosition.x;
		ribbon.position.y = leaderPosition.y;
		ribbon.position.z = leaderPosition.z;
    }

    private void leaderAccelerateToTarget() {
        Vector3 pointingTargetNormalised =
                (new Vector3(target.x - leaderPosition.x, target.y - leaderPosition.y, target.z - leaderPosition.z)).nor();
        leaderVelocity.add(pointingTargetNormalised.scl(leaderSpeed * ACCEL_TARGET_FRACTION));
    }

}
