package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

public class RandomPositionsWithinRadiusSharedAxis extends TransformingFormation {

    static private float MAX_DISPLACEMENT = 180.0f;

    public RandomPositionsWithinRadiusSharedAxis() {
        MINIMUM_FORMATIONS = 2;
        MAXIMUM_FORMATIONS = 4;
    }

    @Override
    protected void initFormation() {
        int numberOfRibbons = this.ribbons.size();

        for(int i = 0; i < numberOfRibbons; i++) {
            float displacement = rnd.nextFloat() * MAX_DISPLACEMENT;

            Vector3 initialPosition = new Vector3().setToRandomDirection().scl(displacement);
            Vector3 position = this.ribbons.get(i).position.cpy();
            Vector3 velocity = this.ribbons.get(i).velocity.cpy();
            float diameter = this.ribbons.get(i).diameter;
            String scheme = "all";
            float speedMult = 1;

            Ribbon ribbonCopy = new Ribbon(position,velocity,diameter,scheme,speedMult);
            ribbonCopy.rotationAxis = ribbonCopy.rotationAxis.cpy();
            ribbonCopy.position = initialPosition;

            this.flightFormationInitial.add(ribbonCopy);

            this.flightFormationTargetPositions.add(ribbonCopy);

        }
    }


    @Override
    public void updateAllRibbons(float dt) {

        // all of the rotation and scaling happens in here... a LOT happens in here
        updateFormation(dt);

        // set the target position of every ribbon, and then move them towards it, or just move it directly if locked already
        for(int i = 0; i < ribbons.size(); i++) {

            // ribbons list is corresponding to flightFormationInitial list
            Ribbon ribbon = ribbons.get(i);
            Ribbon ribbonTargetPosition = this.flightFormationTargetPositions.get(i);

            Vector3 oldPosition = ribbon.position.cpy();
            Vector3 towardsTarget = ribbonTargetPosition.position.cpy().sub(ribbon.position);

            // guyy debug.. TODO: fix. shouldn't always be locked of course.
            locked[i] = true;

            // copy
            if(locked[i] == true) {
                ribbon.position.x = ribbonTargetPosition.position.x;
                ribbon.position.y = ribbonTargetPosition.position.y;
                ribbon.position.z = ribbonTargetPosition.position.z;

                //            System.out.println("ii: " + i + ", " + r.position.x + ", " + r.position.y + ", " + r.position.z + ", " + formationCentreVelocity.len());

                // TODO
                // probably have to have some ribbon.rotationaxis calculation around here for the streamersrenderer
                // AND SET r.rotationAxis HERE!
                // r.rotationAxis = f.rotationAxis; // ?? ... cpy??
            }
            else {
/*
                System.out.println("ii: xx");

                // steer towards target
                Vector3 towardsTargetVelocity = towardsTarget.cpy().nor().scl(approachSpeed * speedSettingFloat);
                if(towardsTargetVelocity.len() * dt > towardsTarget.len()) {
                    locked[i] = true;
                }
                else {
                    ribbon.velocity.add(towardsTargetVelocity);
                    ribbon.velocity.nor().scl(approachSpeed * speedSettingFloat);
                    ribbon.updatePosition(dt);
                }
 */
            }

            // for color calculation
            float distanceTravelled = oldPosition.cpy().dst(ribbon.position);
            float ribbonSpeed = distanceTravelled / dt;

            ribbon.speed = ribbonSpeed;
            ribbon.velocity.nor().scl(ribbonSpeed);

            approachSpeed *= 1.000001f;
        }
    }


    private void updateFormation(float dt) {
        // integrate velocity
        formationCentrePosition.x += formationCentreVelocity.x * dt;
        formationCentrePosition.y += formationCentreVelocity.y * dt;
        formationCentrePosition.z += formationCentreVelocity.z * dt;

        // randomise rotation rate
        if(rnd.nextFloat() < 0.1) {
            // pitchAcceleration   = -10 + rnd.nextFloat() * 20;
//            this.rotationRate += -0.1 + rnd.nextFloat() * 0.2;
        }

        // randomise rotation axis direction
        if(rnd.nextFloat() < 0.1) {
            //          randomiseAxisDirection(this.formationRotationAxis, 0.0f, MAX_FORMATION_ROTATION_RATE * dt * 0.01f, true);
        }

        // randomise scale change rate
        if(rnd.nextFloat() < 0.1) {
            this.scaleRate +=  -0.1 + rnd.nextFloat() * 0.2;
        }

        // integrate rotation rate
        this.rotation += this.rotationRate * dt;

        // integrate scale rate
        //this.scale += this.scaleRate;


        for(int i = 0; i < this.flightFormationInitial.size(); i++) {

            Vector3 position = this.flightFormationInitial.get(i).position.cpy();
            Vector3 velocity = this.flightFormationInitial.get(i).velocity.cpy();
            float diameter = this.flightFormationInitial.get(i).diameter;
            String scheme = "all";
            float speedMult = 1;

            // copy ribbon to get its properties, aside from the ones we are going to change
            Ribbon ribbonInitialCopy = new Ribbon(position, velocity, diameter, scheme, speedMult);

            // make copy because flightFormationInitial must not be altered
            ribbonInitialCopy.position = ribbonInitialCopy.position.cpy();

            // rotate
            ribbonInitialCopy.position.rotate(this.formationRotationAxis,this.rotation);

            // scale
            ribbonInitialCopy.position.scl(this.scale);

            // add centreposition!!!!
//            System.out.println("before: " + i + ", " + ribbonInitialCopy.position.x);
            ribbonInitialCopy.position.add(this.formationCentrePosition.cpy());
            //          System.out.println("after:  " + i + ", " + ribbonInitialCopy.position.x);

            // todo: copy or generate rotation axis

            this.flightFormationTargetPositions.set(i, ribbonInitialCopy);
        }


        // begin steer the whole formation
        randomiseAxisDirection(steeringAxis, 0.0f, TRAJECTORY_TURNING_SPEED_MAX_DELTA * dt * speedSettingFloat,true);
        int direction = rnd.nextBoolean() ? 1 : -1;
        trajectoryTurningSpeed += TRAJECTORY_TURNING_SPEED_MAX_DELTA * rnd.nextFloat() * direction * speedSettingFloat;
        formationCentreVelocity.rotate(steeringAxis, trajectoryTurningSpeed * dt);
        // end steer the whole formation




        // if last time we looked not all were locked, see if all are locked now
        if(allLocked == false) {
            boolean locked = true;
            for (int i = 0; i < this.locked.length; i++) {
                if(this.locked[i] == false) {
                    locked = false;
                    break;
                }
            }
            if(locked == true) {
                allLocked = true;
            }
        }

        if(
                (
                        (formationCentrePosition.x > BOUNDARY_RIGHT ) ||
                                (formationCentrePosition.x < BOUNDARY_LEFT  ) ||
                                (formationCentrePosition.y > BOUNDARY_BOTTOM) ||
                                (formationCentrePosition.y < BOUNDARY_TOP   ) ||
                                (formationCentrePosition.z > BOUNDARY_BACK  ) ||
                                (formationCentrePosition.z < BOUNDARY_FRONT )
                )
        ) {
            centreAccelerateHome(dt);
        }
        approachSpeedLimit();
    }


}
