package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 25/08/17.
 */

/*
** There should usually only be one instance of this class. The methods in here could just as easily be static.
*/

public class OrbRenderer extends FrameBufferBasedRenderer implements Renderer {

    Texture blanker;
    Texture glow;
    static float[] points = new float[20];
    static float blankerColor;
    Random rnd = new Random();
    float xOffset;
    float yOffset;
    double frame = 0;

    int targetFps;
    SpriteBatch sb;
    int UPS;



    @Override
    public void drawAll(int width, int height, int currentFrame, ArrayList<Formation> activeFormations) {
        if (currentFrame % targetFps * 4 == 0) {
            sb.dispose();
            sb = new SpriteBatch();
        }

        sb.begin();

        sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);

        float delta = (1f / UPS);
        for(int k = 0; k * targetFps < UPS; k++) {

            for(int i = 0; i < activeFormations.size(); i++) {

                activeFormations.get(i).updateAllRibbons((float)delta);

                // TODO surely this is redundant?
                ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

            }

        }

        sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        for(int i = 0; i < activeFormations.size(); i++) {

            ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

                for(int j = 0; j < formationRibbons.size(); j++) {
                              draw(formationRibbons.get(j), sb, width, height);
                        }
        }

        sb.end();

    }

    public void draw(Ribbon ribbon, SpriteBatch sb, int width, int height) {
        if(ribbon.position.z <= 0) { return; }   // near z clipping

        int shortSideLength = Math.min(width,height);
        float diameter = ((ribbon.diameter * shortSideLength) / (float)ribbon.position.z);

        float dxh = ribbon.position.x;
        float dyh = ribbon.position.y;
        float xc = (width / 2f)  + ((dxh) / ribbon.position.z) * shortSideLength;
        float yc = (height / 2f) + ((dyh) / ribbon.position.z) * shortSideLength;

        float x1 = xc - diameter / 2.0f;
        float y1 = yc - diameter / 2.0f;
        float x2 = x1 + diameter;
        float y2 = y1 + diameter;


        if (
                x1 > width  |
                        x2 < 0      |
                        y1 > height |
                        y2 < 0
                )
            return;

        float ribbonSpeed = ribbon.speed * 12;
        float alphaFactor = 0.888f;

        if(alphaFactor < 0.0f) {
            alphaFactor = 0.0f;
        }

        float colori = com.badlogic.gdx.graphics.Color.toFloatBits(
                ribbon.r * ribbonSpeed, ribbon.g * ribbonSpeed, ribbon.b * ribbonSpeed, (float)alphaFactor
        );

        points[0] = x1 ;
        points[1] = y1 ;
        points[2] = colori;
        points[5] = x2 ;
        points[6] = y1 ;
        points[7] = colori;
        points[10] = x2 ;
        points[11] = y2 ;
        points[12] = colori;
        points[15] = x1 ;
        points[16] = y2 ;
        points[17] = colori;

        sb.draw(glow, points, 0, 20);



        // white center
        diameter *= 0.2f;
        x1 = xc - diameter / 2.0f;
        y1 = yc - diameter / 2.0f;
        x2 = x1 + diameter;
        y2 = y1 + diameter;

        colori = com.badlogic.gdx.graphics.Color.toFloatBits(
                1,1,1, 0.99f
        );

        points[0] = x1 ;
        points[1] = y1 ;
        points[2] = colori;
        points[5] = x2 ;
        points[6] = y1 ;
        points[7] = colori;
        points[10] = x2 ;
        points[11] = y2 ;
        points[12] = colori;
        points[15] = x1 ;
        points[16] = y2 ;
        points[17] = colori;

        sb.draw(glow, points, 0, 20);

    }

    public void blankOut(SpriteBatch sb,int width, int height) {

    }

    @Override
    public void removeRibbon(Ribbon r) {

    }

    public void generateBlanker(float speedSetting) {

        blankerColor = com.badlogic.gdx.graphics.Color.toFloatBits(
                0, 0, 0, 0.33f      // normal
        );

        Pixmap p1 = new Pixmap(16,16, Pixmap.Format.RGBA8888);
        p1.setColor(0f, 0f, 0f, 1f);
        p1.fill();
        blanker = new Texture(p1);
        p1.dispose();
    }

    public OrbRenderer(int targetFps, int UPS, String imageQuality, String filtering, int width, int height, int diameter) {
        String prefix = "";
        String filepath = new String(prefix + "glow" + ".png");
        FileHandle handle = Gdx.files.internal(filepath);
        glow = new Texture(handle, Pixmap.Format.RGBA8888, false);

        this.targetFps = targetFps;
        this.UPS = UPS;

        this.initialiseFrameBuffer(imageQuality, width, height);

        this.sb = new SpriteBatch();

        Texture.TextureFilter filter = filtering.equals("nearest") ? Texture.TextureFilter.Nearest : Texture.TextureFilter.Linear;
        fb.getColorBufferTexture().setFilter(filter,filter);

        Gdx.gl20.glDisable(GL20.GL_DEPTH_TEST);

        points[3] = 0;
        points[4] = 0;
        points[8] = 1;
        points[9] = 0;
        points[13] = 1;
        points[14] = 1;
        points[18] = 0;
        points[19] = 1;
    }
}

