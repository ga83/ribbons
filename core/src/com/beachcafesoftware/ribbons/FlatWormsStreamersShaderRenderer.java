package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ImmediateModeRenderer20;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Guy Aaltonen on 5/01/19.
 */
// TODO: touch up logic to render a ribbon even if it's not active anymore. as of now, when a ribbon is replaced,
// TODO: the whole thing disappears at once. it would be nice for it to shrink and then disappear.
// TODO: ribbon only gets replaced every 90 seconds, so it's not urgent, but it would look better.
public class FlatWormsStreamersShaderRenderer implements Renderer {

    private static float RIBBON_WIDTH;
//    private static final float RIBBON_MAX_RUNGS = 50.0f;
    private static final float RIBBON_MAX_RUNGS = 66.0f;
    private final int UPS;
    private final int targetFps;

    private static final int NUMBER_OF_POSITION_COMPONENTS = 3;	// xyz
//    private static final int NUMBER_OF_COLOR_COMPONENTS = 3;		// rgb
    private static final int NUMBER_OF_COLOR_COMPONENTS = 4;		// rgb
//    private static final int NUMBER_OF_NORMAL_COMPONENTS = 3;	// xyz vector
    private static final int NUMBER_OF_TEXTURE_COORDINATE_COMPONENTS = 2;   // xy
    private static final int NUM_COMPONENTS = NUMBER_OF_POSITION_COMPONENTS + NUMBER_OF_COLOR_COMPONENTS + NUMBER_OF_TEXTURE_COORDINATE_COMPONENTS;

//    private static final int MAX_TRIS = 5460;
    private static final int MAX_TRIS = 5460;
    private static final int MAX_VERTS = MAX_TRIS * 3;

    // immediate mode opengl fields
    protected static float[] verts;
    protected static int index = 0;
    private final Texture margot;
    protected ShaderProgram shaderProgram;
    protected Mesh mesh;
    protected static float[] lightPos = new float[] { 0, 0, 0 };	// this seems to be relative to the screen / camera in the shaderProgram, so the light sources "follow" the screen. not necessarily wwhat we want, but if we want to change it we might have to transform its position in the vertex shaderProgram maybe?

    PerspectiveCamera cam;

    ImmediateModeRenderer20 renderer;
    float color;

    static Vector3 p1;
    static Vector3 p2;
    static Vector3 p3;
    static Vector3 p4;

    Map<Ribbon,ArrayList<Rung>> rungMap = new HashMap<Ribbon, ArrayList<Rung>>();


    @Override
    public void drawAll(int width, int height, int currentFrame, ArrayList<Formation> activeFormations) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();

        float delta = 1f / UPS;
        for(int k = 0; k * targetFps < UPS; k++) {
            for(int i = 0; i < activeFormations.size(); i++) {
                activeFormations.get(i).updateAllRibbons((float)delta);
            }
        }

        for(int i = 0; i < activeFormations.size(); i++) {
            ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

            for(int j = 0; j < formationRibbons.size(); j++) {
                draw(formationRibbons.get(j));
            }
        }

        for(int i = 0; i < activeFormations.size(); i++) {
            ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

            for (int j = 0; j < formationRibbons.size(); j++) {
                Ribbon ribbon = formationRibbons.get(j);
                ArrayList rungList = rungMap.get(ribbon);
                int rungListLength;
                rungListLength = rungList.size();

                if (
                        rungListLength % 2 == 0 &&
                                rungListLength > (RIBBON_MAX_RUNGS * (targetFps / 30.0f) )
                )
                {
                    rungList.remove(0);
                    rungList.remove(0);
                    rungListLength = rungList.size();
                }

                for (int k = 1; k < rungListLength; k++) {
                    Rung frontRung = rungMap.get(ribbon).get(k);
                    Rung rearRung  = rungMap.get(ribbon).get(k - 1);

                    float frontRungLength = (rungListLength / RIBBON_MAX_RUNGS) * RIBBON_WIDTH;
                    float rearRungLength  = (rungListLength / RIBBON_MAX_RUNGS) * RIBBON_WIDTH;

                    float period = rungListLength * 2;

                    float frontPeriodicComponent = (float) Math.sin((k / period) * (2 * Math.PI));
                    float rearPeriodicComponent = (float) Math.sin(((k - 1) / period) * (2 * Math.PI));

                    float closeness = 1f / (frontRung.rungCentrePosition.z / 8.0f);

                    // old, no fade out
//                     float lum = Math.min(closeness * 200 , 1.0f);

                    // new, looks better for front end back to fade out
                    //float lum = Math.min(closeness * 200 , 1.0f) * frontPeriodicComponent;

                    // newer still
                    float hueOffset = k / (float)rungListLength;

                    float lum = Math.min(closeness * 200 , 1.0f) * frontPeriodicComponent;

                    float r = Math.min(ribbon.r * 2000 * lum, 1.0f);
                    float g = Math.min(ribbon.g * 2000 * lum, 1.0f);
                    float b = Math.min(ribbon.b * 2000 * lum, 1.0f);

                    com.badlogic.gdx.graphics.Color ribbonColor = new com.badlogic.gdx.graphics.Color(r, g, b, hueOffset);

                    p1 = frontRung.rungCentrePosition.cpy().add(frontRung.rungDirection.cpy().scl( (frontRungLength * frontPeriodicComponent)));
                    p2 = frontRung.rungCentrePosition.cpy().add(frontRung.rungDirection.cpy().scl( (frontRungLength * frontPeriodicComponent * -1.0f)));
                    p3 = rearRung.rungCentrePosition.cpy() .add(rearRung .rungDirection.cpy().scl( (rearRungLength  * rearPeriodicComponent)));
                    p4 = rearRung.rungCentrePosition.cpy() .add(rearRung .rungDirection.cpy().scl( (rearRungLength  * rearPeriodicComponent * -1.0f)));


                    // works pre-texture, but we want texture
                    /*
                    // see if we can reuse, since the vertices written don't depend
                    WallpaperTriangle t1 = new WallpaperTriangle(p1, p2, p3, ribbonColor, towardsScreen, margot);
                    addTriangle(t1);

                    WallpaperTriangle t2 = new WallpaperTriangle(p3, p2, p4, ribbonColor, towardsScreen, margot);
                    addTriangle(t2);
                    */


                    // new, works with texture
                    addTriangle(p1, p3, p2, ribbonColor, true);
                    addTriangle(p4, p2, p3, ribbonColor, false);
                }
            }
        }

        flush();
    }

    public void draw(Ribbon ribbon) {
        if(rungMap.get(ribbon) == null) {
            rungMap.put(ribbon, new ArrayList<Rung>());
        }

        Rung rung;
        rung = new Rung();
        rung.rungCentrePosition = ribbon.position.cpy();
        rung.rungDirection = ribbon.rotationAxis.cpy();
        rungMap.get(ribbon).add(rung);
    }

    private void addTriangle(Vector3 v1, Vector3 v2, Vector3 v3, com.badlogic.gdx.graphics.Color color, boolean topTriangle) {

        if (index == verts.length) {
            flush();
        }

        // vertex culling
        if(cam.frustum.sphereInFrustumWithoutNearFar(v3, 3000) == false) {
            return;
        }

        float r = color.r;
        float g = color.g;
        float b = color.b;
        float hueShift = color.a;

        //bottom left vertex
        // position
        verts[index++] = v1.x; 			//Position(x, y)
        verts[index++] = v1.y;
        verts[index++] = v1.z;

        // color
        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;
        verts[index++] = hueShift;

        // margot
        if(topTriangle == true) {
            verts[index++] = 0;
            verts[index++] = 0;
        } else {
            verts[index++] = 1;
            verts[index++] = 1;
        }


        //top left vertex
        // position
        verts[index++] = v2.x; 			//Position(x, y)
        verts[index++] = v2.y;
        verts[index++] = v2.z;

        // color
        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;
        verts[index++] = hueShift;


        // margot
        if(topTriangle == true) {
            verts[index++] = 1;
            verts[index++] = 0;
        } else {
            verts[index++] = 0;
            verts[index++] = 1;
        }


        //bottom right vertex
        // position
        verts[index++] = v3.x;	 //Position(x, y)
        verts[index++] = v3.y;
        verts[index++] = v3.z;

        // color
        verts[index++] = r; 	//Color(r, g, b, a)
        verts[index++] = g;
        verts[index++] = b;
        verts[index++] = hueShift;


        // margot
        if(topTriangle == true) {
            verts[index++] = 0;
            verts[index++] = 1;
        } else {
            verts[index++] = 1;
            verts[index++] = 0;
        }

    }

    private void flush() {

        //if we've just flushed, don't flush again
        if (index == 0) {
            return;
        }

        mesh.setVertices(verts);

        //number of vertices we need to render
        int vertexCount = index / NUM_COMPONENTS;

        //start the shaderProgram before setting any uniforms
        shaderProgram.begin();

        // is this where we do it?
        margot.bind();


        // light position, otherwise nothing will appear
        shaderProgram.setUniform3fv("LightPos",lightPos,0,3);

        //update the projection matrix so our triangles are rendered in 2D
        shaderProgram.setUniformMatrix("u_projModelView", cam.combined);

        //render the mesh
        mesh.render(shaderProgram, GL20.GL_TRIANGLES, 0, vertexCount);

        shaderProgram.end();

        //reset index to zero
        index = 0;
    }



    @Override
    public void generateBlanker(float speedSetting) {

    }

    @Override
    public void blankOut(SpriteBatch sb, int width, int height) {

    }

    @Override
    public void removeRibbon(Ribbon r) {
        // TODO: because we keep references to ribbons in this renderer, remove our own reference.
        // TODO: however, we should still putCloudPuffs the "expired" ribbon, and reduce its size,
        // TODO: until there is none left. it looks better than removing the whole thing at once.
        // TODO: this is a temporary hack.
        // TODO: probably move the ribbon into a temporary list for expired ribbons.
        // TODO: and of course render those, and reduce their size each frame.
        // TODO: ribbons get replaced every 90 seconds so it's not a huge deal.
        // TODO: but it would still be preferable.
        this.rungMap.remove(r);

    }

    public FlatWormsStreamersShaderRenderer(int targetFps, int UPS, String imageQuality, String filtering, int width, int height, int diameter) {
        cam = new PerspectiveCamera(90, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.x = 0;
        cam.position.y = 0;
        cam.position.z = 0;
        cam.lookAt(0,0,1f);
        cam.near = 0.1f;   // must be greater than 0 otherwise depth test fails frequently
        cam.far = 6000;

        mesh = new Mesh(
                true,
                MAX_VERTS,
                0,
                new VertexAttribute(VertexAttributes.Usage.Position, NUMBER_OF_POSITION_COMPONENTS, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.ColorUnpacked, NUMBER_OF_COLOR_COMPONENTS, "a_color"),
//                new VertexAttribute(VertexAttributes.Usage.Normal, NUMBER_OF_NORMAL_COMPONENTS, "a_normal"),
                new VertexAttribute(VertexAttributes.Usage.Normal, NUMBER_OF_TEXTURE_COORDINATE_COMPONENTS, "a_texCoords")
        );

        verts = new float[MAX_VERTS * NUM_COMPONENTS];

        String vertexShaderString = Gdx.files.internal("vertex-worms-transparent.glsl").readString();
        String fragmentShaderString = Gdx.files.internal("fragment-worms-transparent.glsl").readString();

        ShaderProgram.pedantic = false;
        shaderProgram = new ShaderProgram(vertexShaderString, fragmentShaderString);

        String log = shaderProgram.getLog();

        if (shaderProgram.isCompiled() == false) {
            throw new GdxRuntimeException(log);
        }

        if (log != null)
            System.out.println("Shader compile log: " + log);


//        FileHandle margot = Gdx.files.internal("fadeoutwards.png");
        FileHandle margot = Gdx.files.internal("fadetwice.png");
//        FileHandle margot = Gdx.files.internal("binary.png");
//        FileHandle margot = Gdx.files.internal("poop.png");

        Pixmap pixmap = new Pixmap(margot);
        this.margot = new Texture(pixmap);
        pixmap.dispose();

        this.targetFps = targetFps;
        this.UPS = UPS;

        RIBBON_WIDTH = 8.0f * diameter / 60.0f;

        Gdx.gl20.glDisable(GL20.GL_DEPTH_TEST);
        Gdx.gl20.glDepthMask(false);

        Gdx.gl20.glEnable(GL20.GL_BLEND);
        Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
    }

}
