package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

public class Utilities {

    private static Vector3 screenFacingNormal = new Vector3(0,0,-1);
//    private static Vector3 screenFacingNormal = new Vector3(0,0,1);


    public static Vector3 getScreenFacingNormal(WallpaperTriangle t1, Vector3 specifiedNormal) {

        Vector3 arm1 = t1.v1.cpy().sub(t1.v2).nor();
        Vector3 arm2 = t1.v3.cpy().sub(t1.v2).nor();
        Vector3 normal1 = arm1.cpy().crs(arm2).nor();
        Vector3 normal2 = normal1.cpy().scl(-1f).nor();

        float normal1dot;
        float normal2dot;

        if(specifiedNormal == null) {
            normal1dot = normal1.dot(screenFacingNormal);
            normal2dot = normal2.dot(screenFacingNormal);
        } else {
            normal1dot = normal1.dot(specifiedNormal);
            normal2dot = normal2.dot(specifiedNormal);
        }


        if(normal1dot > normal2dot) {
            // TODO: for some reason, when it comes to lighting the triangle, the x component of the normal is "wrong", but not the y or z. so flip the x... figure out what's really wrong here.
            // TODO: found out that it was drawing everything (eg boxes) inverted in terms of position. so boxes with negative x coords would appear on the right side of the screen.
            // TODO: so the normals weren't wrong. currently have hack in vertex.glsl to just invert the x coord of every vertex. don't need to invert the x direction of the normals.
            // TODO: but even the inversion of values in vertex.glsl is not the real solution.
            //          normal1.x *= -1;
            return normal1;
        } else {
            // TODO: for some reason, when it comes to lighting the triangle, the x component of the normal is "wrong", but not the y or z. so flip the x... figure out what's really wrong here.
            // TODO: found out that it was drawing everything (eg boxes) inverted in terms of position. so boxes with negative x coords would appear on the right side of the screen.
            // TODO: so the normals weren't wrong. currently have hack in vertex.glsl to just invert the x coord of every vertex. don't need to invert the x direction of the normals.
            // TODO: but even the inversion of values in vertex.glsl is not the real solution.
//            normal2.x *= -1;
            return normal2;
        }
    }


}
