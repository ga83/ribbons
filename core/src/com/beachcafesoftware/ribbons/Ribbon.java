package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

import java.util.Random;

/**
 * Created by Guy Aaltonen on 25/08/17.
 */


public class Ribbon {

    static Random rnd = new Random();

    static float BOUNDARY_RIGHT =  1.0f + 400.0f;
    static float BOUNDARY_LEFT =   0.0f - 400.0f;
    static float BOUNDARY_BOTTOM = 1.0f + 400.0f;
    static float BOUNDARY_TOP =    0.0f - 400.0f;
    static float BOUNDARY_FRONT =  -200.0f;
    static float BOUNDARY_BACK =   700.0f;

    public static float MAX_SPEED = 500.0f;
    public static float MIN_SPEED = 400.0f;

    public static float MAX_DELTA_ROTATE = 8.0f;     // degrees per second. set higher for fun.
    public static float ACCEL_HOME_FRACTION = 0.0066f;

    Vector3 position;
    Vector3 velocity;
    float diameter;
    float r;
    float g;
    float b;
    float turningAngle = 0;
    Vector3 rotationAxis;
    boolean returningHome = false;
    float speed;


    public void setRandomSpeed(float speedMult, boolean useSameSpeed) {

        float speed;

        if(useSameSpeed == false) {
            speed = Ribbon.MIN_SPEED + rnd.nextFloat() * (Ribbon.MAX_SPEED - Ribbon.MIN_SPEED);
        } else {
            speed = (Ribbon.MAX_SPEED + Ribbon.MIN_SPEED) / 2;
        }

        if(this.velocity.len() == 0) {
            this.velocity.setToRandomDirection();
        }

        this.velocity.nor().scl(speed * speedMult);
        this.speed = velocity.len();
    }


    public Ribbon(Vector3 position, Vector3 velocity, float diameter, String scheme, float speedMult) {

        this.position = position;
        this.velocity = velocity;
        this.velocity.scl(speedMult);
        this.speed = this.velocity.len();
        this.diameter = diameter;

        float[] color = Color.getRandomColorFromScheme(scheme);

        this.r = color[0];
        this.g = color[1];
        this.b = color[2];

        this.rotationAxis = new Vector3(1,0,0).setToRandomDirection();
    }



    public void updatePosition(float dt) {
        position.x += velocity.x * dt;
        position.y += velocity.y * dt;
        position.z += velocity.z * dt;
    }


    public void approachSpeedLimit() {
        if(velocity.len() > speed) {
//            velocity.scl(0.99f);
            velocity.scl(0.9f);
        }
        else if(velocity.len() < speed) {
  //          velocity.scl(1.0f / 0.99f);
            velocity.scl(1.0f / 0.9f);
        }
    }

    public void accelerateHome() {
        Vector3 pointingHomeNormalised =
                (new Vector3(0 - position.x, 0 - position.y, BOUNDARY_FRONT - position.z)).nor();
        velocity.add(pointingHomeNormalised.scl(speed * ACCEL_HOME_FRACTION));
    }

    public void randomiseRotationAxis(Vector3 rotationAxis, float minAngle, float maxAngle) {
        int axis = rnd.nextInt(3);
        Vector3 rotationAxis2;

        if(axis == 0)      { rotationAxis2 = new Vector3(1,0,0); }
        else if(axis == 1) { rotationAxis2 = new Vector3(0,1,0); }
        else               { rotationAxis2 = new Vector3(0,0,1); }

        int sign = rnd.nextBoolean() ? 1 : -1;
        float range = maxAngle - minAngle;
        float angle = minAngle + rnd.nextFloat() * range * sign;

        rotationAxis.rotate(rotationAxis2,angle);
    }


}
