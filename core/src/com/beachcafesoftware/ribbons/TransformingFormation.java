package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;

public abstract class TransformingFormation implements Formation {

    protected ArrayList<Ribbon> ribbons;

    // untransformed formation - ie the formation exactly as initialised in initFormation
    protected ArrayList<Ribbon> flightFormationInitial = new ArrayList<Ribbon>();

    // this is where the ribbons should be ideally
    // locked ribbons are copied from here straight to this.ribbons
    // not-yet locked ribbons approach the positions of the ribbons in here
    protected ArrayList<Ribbon> flightFormationTargetPositions = new ArrayList<Ribbon>();

    protected float trajectoryTurningSpeed = 0.0f;                               // degrees per second
    protected static float TRAJECTORY_TURNING_SPEED_MAX_DELTA = 0.5f;     // degrees per second per second
    private static float ACCEL_HOME_FRACTION = 0.0066f;
    private static float APPROACH_SPEED = 400;

    protected float approachSpeed = APPROACH_SPEED;

    protected static int MINIMUM_FORMATIONS;
    protected static int MAXIMUM_FORMATIONS;

    protected static final float LEADER_MAX_SPEED = 250.0f;
    protected static final float LEADER_MIN_SPEED = 120.0f;

    static float BOUNDARY_RIGHT =  1.0f + 50.0f;
    static float BOUNDARY_LEFT =   0.0f - 50.0f;
    static float BOUNDARY_BOTTOM = 1.0f + 50.0f;
    static float BOUNDARY_TOP =    0.0f - 50.0f;
    static float BOUNDARY_FRONT =  600.0f;
    static float BOUNDARY_BACK =   1000.0f;

    float leaderSpeed;
    boolean returningHome;
    protected Random rnd = new Random();
    private int lastReplacedIndex;
    protected float speedSettingFloat;

    protected boolean[] locked;
    protected boolean allLocked;

    protected Vector3 steeringAxis;               // steer the formation as it moves

    protected Vector3 formationCentrePosition;     // absolute position of centre, as in normal ribbon's position
    protected Vector3 formationCentreVelocity;     // absolute velocity of centre, as in normal ribbon's velocity
    protected Vector3 formationRotationAxis;   // axis around which entire formation rotates

    protected float rotation;         //  r
    protected float rotationRate;     // dr / dt
    protected float scale;            //  s
    protected float scaleRate;        // ds / dt


    // every subclass needs to override this because this is where the basic shape of the formation is defined
    protected abstract void initFormation();


    public TransformingFormation() {

        formationCentrePosition = new Vector3(
                -100 + rnd.nextFloat() * 200,
                -100 + rnd.nextFloat() * 200,
                400 + rnd.nextFloat() * 500
        );

        leaderSpeed = LEADER_MIN_SPEED;
        returningHome = false;
        ribbons = new ArrayList<Ribbon>();
        lastReplacedIndex = 0;
        allLocked = false;

        steeringAxis = new Vector3().setToRandomDirection();
        formationRotationAxis = new Vector3().setToRandomDirection();

        rotation = 0.0f;
        rotationRate = 360.0f;
        scale = 1.0f;
        scaleRate = 0.1f;
    }

    @Override
    public void setSpeed(float speed) {
        this.speedSettingFloat = speed;
        float leaderSpeed = LEADER_MIN_SPEED + rnd.nextFloat() * (LEADER_MAX_SPEED - LEADER_MIN_SPEED);
        this.formationCentreVelocity = new Vector3().setToRandomDirection().scl(leaderSpeed);
    }

    @Override
    public void removeAllRibbons() {
        this.ribbons.clear();
    }

    @Override
    public void addRibbons(ArrayList<Ribbon> ribbons,float speedMult) {
        // do something with the speedMult because it should apply before the ribbons are locked in place
        this.ribbons.addAll(ribbons);
        locked = new boolean[ribbons.size()];
        this.initFormation();
    }



    public void randomiseAxisDirection(Vector3 rotationAxis, float minAngle, float maxAngle, boolean negativePossible) {
        int axis = rnd.nextInt(3);
        Vector3 rotationAxis2;

        if(axis == 0)      { rotationAxis2 = new Vector3(1,0,0); }
        else if(axis == 1) { rotationAxis2 = new Vector3(0,1,0); }
        else               { rotationAxis2 = new Vector3(0,0,1); }

        int sign;
        if(negativePossible == true) {
            sign = rnd.nextBoolean() ? 1 : -1;
        }
        else {
            sign = 1;
        }
        float range = maxAngle - minAngle;
        float angle = minAngle + rnd.nextFloat() * range * sign;

        rotationAxis.rotate(rotationAxis2,angle);
    }


    protected void centreAccelerateHome(float dt) {
        Vector3 pointingHomeNormalised = (new Vector3(0 - formationCentrePosition.x, 0 - formationCentrePosition.y, BOUNDARY_FRONT - formationCentrePosition.z)).nor();
        formationCentreVelocity.add(pointingHomeNormalised.scl(leaderSpeed * ACCEL_HOME_FRACTION));
    }

    @Override
    public ArrayList<Ribbon> getRibbons() {
        return ribbons;
    }

    @Override
    public int getNextReplacedIndex() {
        lastReplacedIndex ++;
        if(lastReplacedIndex >= ribbons.size()) {
            lastReplacedIndex = 0;
        }
        return lastReplacedIndex;
    }

    @Override
    public int getMinimumFormations() {
        return MINIMUM_FORMATIONS;
    }

    @Override
    public int getMaximumFormations() {
        return MAXIMUM_FORMATIONS;
    }

    public void approachSpeedLimit() {
        if(formationCentreVelocity.len() > leaderSpeed) {
            formationCentreVelocity.scl(0.99f);
        }
        else if(formationCentreVelocity.len() < leaderSpeed) {
            formationCentreVelocity.scl(1.0f / 0.99f);
        }
    }

}
