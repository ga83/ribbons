package com.beachcafesoftware.ribbons;

/**
 * Created by Guy Aaltonen on 5/09/18.
 */

public class ParametersNotSetException extends Exception {

    public ParametersNotSetException() {

    }

    @Override
    public String getMessage() {
        return "IP or intterval not set";
    }
}
