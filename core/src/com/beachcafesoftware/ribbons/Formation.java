package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.ArrayList;

/**
 * Created by Guy Aaltonen on 1/11/17.
 */

public interface Formation {
    public int getMinimumFormations();
    public int getMaximumFormations();
    public void removeAllRibbons();
    public void addRibbons(ArrayList<Ribbon> ribbons, float speed);
    public void setSpeed(float speed);     // called once, might not be needed if we can do everything in constructor
    public void updateAllRibbons(float dt);   // called every frame
    public ArrayList<Ribbon> getRibbons();
    public int getNextReplacedIndex();
}
