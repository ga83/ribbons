package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 5/01/19.
 */

// TODO: touch up logic to render a ribbon even if it's not active anymore. as of now, when a ribbon is replaced,
// TODO: the whole thing disappears at once. it would be nice for it to shrink and then disappear.
// TODO: ribbon only gets replaced every 90 seconds, so it's not urgent, but it would look better.
public class ParticlesRenderer extends FrameBufferBasedRenderer implements Renderer {

    public static final float SPRITE_LIFE = 7.0f;
    private ArrayList<Particle> particles = new ArrayList<Particle>();
    private final int UPS;
    private final int targetFps;
    float color;
    SpriteBatch sb;
    Texture star;
    static float[] points = new float[20];
    private float RIBBON_DIAMETER;
    private ParticleComparator puffComparator = new ParticleComparator();
    Random rnd = new Random();


    @Override
    public void drawAll(int width, int height, int currentFrame, ArrayList<Formation> activeFormations) {

        for (int k = 0; k * targetFps < UPS; k++) {

            float delta = (1f / UPS);

            for (int i = 0; i < activeFormations.size(); i++) {
                activeFormations.get(i).updateAllRibbons(delta);

                ArrayList<Ribbon> formationRibbons = activeFormations.get(i).getRibbons();

                for (int j = 0; j < formationRibbons.size(); j++) {
                    putParticles(formationRibbons.get(j));
                }
            }
        }

        sortParticles();

        drawParticles(currentFrame, width, height);
    }

    private void sortParticles() {
        particles.sort(puffComparator);
    }

    private void drawParticles(int currentFrame, int width, int height) {

        if (currentFrame % targetFps * 4 == 0) {
            sb.dispose();
            sb = new SpriteBatch();
        }

        // draw ribbons onto framebuffer
        fb.begin();

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sb.begin();

        // remove dead puffs
        int c = 0;

        if(targetFps == 30 || (currentFrame % 2 == 0)) {
            while (c < particles.size()) {
                Particle particle = particles.get(c);
                particle.life -= 0.1;

                particle.position.add(particle.velocity);

                if (particle.life < 0) {
                    particles.remove(c);
                } else {
                    c++;
                }
            }
        }

        for(int i = 0; i < particles.size(); i++) {
            Particle particle = particles.get(i);

            float reciprocalOfZ = 1f / particle.position.z;
            int shortSideLength = Math.min(width,height);

            float diameterSmall = ((RIBBON_DIAMETER * shortSideLength) * reciprocalOfZ) * particle.life;

            float colori = particle.color;
            float dxh = particle.position.x;
            float dyh = particle.position.y;
            float xc = (width * 0.5f)  + ((dxh) * reciprocalOfZ) * shortSideLength;
            float yc = (height * 0.5f) + ((dyh) * reciprocalOfZ) * shortSideLength;

            float x1 = xc - diameterSmall * 0.5f;
            float y1 = yc - diameterSmall * 0.5f;
            float x2 = x1 + diameterSmall;
            float y2 = y1 + diameterSmall;

            points[0] = x1 ;
            points[1] = y1 ;
            points[2] = colori;
            points[5] = x2 ;
            points[6] = y1 ;
            points[7] = colori;
            points[10] = x2 ;
            points[11] = y2 ;
            points[12] = colori;
            points[15] = x1 ;
            points[16] = y2 ;
            points[17] = colori;

            sb.draw(star, points, 0, 20);
        }

        sb.end();
        fb.end();

        // draw framebuffer onto screen
        sb.begin();
        sb.draw(fb.getColorBufferTexture(), 0f, 0f, width, height);
        sb.end();

    }

    public void putParticles(Ribbon ribbon) {
        putParticle(ribbon);
    }


    public void putParticle(Ribbon ribbon) {
        if(ribbon.position.z < 0) { return; }   // near z clipping

        float alphaFactor = 1.0f;

        float colori = com.badlogic.gdx.graphics.Color.toFloatBits(
                ribbon.r * 2000, ribbon.g * 2000, ribbon.b * 2000, alphaFactor
        );

        Particle particle = new Particle();
        particle.position = ribbon.position.cpy();
        particle.life = SPRITE_LIFE;
        particle.velocity = new Vector3().setToRandomDirection().scl(ribbon.speed * 0.00133f);
        particle.color = colori;

        particles.add(particle);
    }

    @Override
    public void generateBlanker(float speedSetting) {

    }

    @Override
    public void blankOut(SpriteBatch sb, int width, int height) {

    }

    @Override
    public void removeRibbon(Ribbon r) {
        // TODO: because we keep references to ribbons in this renderer, remove our own reference.
        // TODO: however, we should still putCloudPuffs the "expired" ribbon, and reduce its size,
        // TODO: until there is none left. it looks better than removing the whole thing at once.
        // TODO: this is a temporary hack.
        // TODO: probably move the ribbon into a temporary list for expired ribbons.
        // TODO: and of course render those, and reduce their size each frame.
        // TODO: ribbons get replaced every 90 seconds so it's not a huge deal.
        // TODO: but it would still be preferable.
    }

    public ParticlesRenderer(int targetFps, int UPS, String imageQuality, String filtering, int width, int height, int diameter) {

        String prefix = "";
        String filepath = new String(prefix + "glow8" + ".png");
        FileHandle handle = Gdx.files.internal(filepath);
        star = new Texture(handle, Pixmap.Format.RGBA8888, false);

        this.targetFps = targetFps;
        this.UPS = UPS;

        this.initialiseFrameBuffer(imageQuality, width, height);

        this.sb = new SpriteBatch();

        Texture.TextureFilter filter = filtering.equals("nearest") ? Texture.TextureFilter.Nearest : Texture.TextureFilter.Linear;
        fb.getColorBufferTexture().setFilter(filter,filter);

        RIBBON_DIAMETER = diameter / 16.0f;

        Gdx.gl20.glDisable(GL20.GL_DEPTH_TEST);

        Gdx.gl20.glEnable(GL20.GL_BLEND);
        Gdx.gl20.glBlendEquation(GL20.GL_FUNC_ADD);

        Gdx.gl20.glBlendFuncSeparate
                (
                        GL20.GL_SRC_ALPHA,
                        GL20.GL_ONE_MINUS_SRC_ALPHA,
                        GL20.GL_SRC_ALPHA,
                        GL20.GL_ONE
                );

        points[3] = 0;
        points[4] = 0;
        points[8] = 1;
        points[9] = 0;
        points[13] = 1;
        points[14] = 1;
        points[18] = 0;
        points[19] = 1;


    }

}
