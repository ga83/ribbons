package com.beachcafesoftware.ribbons;

import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 3/11/17.
 */


// TODO: the rotation axis probably isn't set each frame. it needs to be correct, because the ShardsRenderer relies on the rotation axis to render properly.
// TODO: however, calculating rotationaxis every frame will be wasting cpu when using renderer that doen't need it.
// TODO: it seems crazy, but i think we should make copies of all formations, one with that calculation, and one without.
public abstract class FlightFormation implements Formation {

    // raison d'etre
    // these may have to be ArrayList<Ribbon> 's... because the ribbon's
    // rotation axis needs to be tracked, not only its position.

    // positions of the formation around the origin. it exists because it's easier to rotate around the origin than to translate back to origin before rotating, and then translating back, each frame.
    protected ArrayList<Vector3> flightFormationRotatedButNotTranslated = new ArrayList<Vector3>();

    // positions of the formation after translating it to world space. it is recaculated every frame because the position and rotation probably have changed.
    protected ArrayList<Vector3> flightFormationTargetPositionsAfterTranslation = new ArrayList<Vector3>();

    // where the actual ribbons are... the physical location of them... ie where they end up being drawn.
    protected ArrayList<Ribbon> ribbons;

    private static float APPROACH_SPEED = 400;
    private float approachSpeed = APPROACH_SPEED;

    protected static int MINIMUM_FORMATIONS;
    protected static int MAXIMUM_FORMATIONS;

    public static float MIN_SPEED = 200.0f;

    static float BOUNDARY_RIGHT =  1.0f + 500.0f;
    static float BOUNDARY_LEFT =   0.0f - 500.0f;
    static float BOUNDARY_BOTTOM = 1.0f + 500.0f;
    static float BOUNDARY_TOP =    0.0f - 500.0f;
    static float BOUNDARY_FRONT =  400.0f;
    static float BOUNDARY_BACK =   1000.0f;


    private Vector3 centrePosition;     // absolute position of centre, as in normal ribbon's position
    private Vector3 formationCentreVelocity;     // absolute velocity of centre, as in normal ribbon's velocity

    private Vector3 pitchAxis = new Vector3(1.0f, 0.0f, 0.0f);
    private Vector3 yawAxis =   new Vector3(0.0f, 1.0f, 0.0f);
    private Vector3 rollAxis =  new Vector3(0.0f, 0.0f, 1.0f);

    /*
    private static final float MAX_PITCH_RATE_CHANGE = 150.0f;
    private static final float MIN_PITCH_RATE_CHANGE = -150.0f;
    private static final float MAX_YAW_RATE_CHANGE = 150.0f;
    private static final float MIN_YAW_RATE_CHANGE = -150.0f;
    private static final float MAX_ROLL_RATE_CHANGE = 350.0f;
    private static final float MIN_ROLL_RATE_CHANGE = -350.0f;
    */

    private static final float MAX_PITCH_RATE_CHANGE = 150.0f;
    private static final float MIN_PITCH_RATE_CHANGE = -150.0f;
    private static final float MAX_YAW_RATE_CHANGE = 150.0f;
    private static final float MIN_YAW_RATE_CHANGE = -150.0f;
    private static final float MAX_ROLL_RATE_CHANGE = 350.0f;
    private static final float MIN_ROLL_RATE_CHANGE = -350.0f;


    private static final float MAX_PITCH_RATE =  90f;
    private static final float MIN_PITCH_RATE = -90f;
    private static final float MAX_YAW_RATE =  90f;
    private static final float MIN_YAW_RATE = -90f;
    private static final float MAX_ROLL_RATE =  90f;
    private static final float MIN_ROLL_RATE = -90f;

    private float pitchRate = 0.0f; // degrees per second
    private float yawRate = 0.0f; // degrees per second
    private float rollRate = 0.0f; // degrees per second


    // use these
    private float pitchAcceleration = 0.0f;
    private float yawAcceleration = 0.0f;
    private float rollAcceleration = 0.0f;

    private static Random rnd;

    float leaderSpeed;
    boolean returningHome;
    private int lastReplacedIndex;
    private float speedSettingFloat;

    private boolean[] locked;
    private boolean allLocked;


    // every subclass needs to override this because this is where the basic shape of the formation is defined
    protected abstract void initFormation();

    public FlightFormation() {

        if(rnd == null) {
            rnd = new Random();
        }

        centrePosition = new Vector3(
                -300 + rnd.nextFloat() * 600,
                -300 + rnd.nextFloat() * 600,
                800 + rnd.nextFloat() * 500
        );

        leaderSpeed = MIN_SPEED;
        returningHome = false;
        ribbons = new ArrayList<Ribbon>();
        lastReplacedIndex = 0;
        allLocked = false;
    }



    @Override
    public void setSpeed(float speedScale) {
        float regularRibbonspeed = Ribbon.MIN_SPEED + rnd.nextFloat() * (Ribbon.MAX_SPEED - Ribbon.MIN_SPEED);

        this.speedSettingFloat = speedScale;

        // velocity always starts going into the distance. when subclassing, keep this in mind when putting down initial positions.
        this.formationCentreVelocity = new Vector3(0,0,1).scl(regularRibbonspeed);
        this.formationCentreVelocity.scl(speedScale);


        this.leaderSpeed = this.formationCentreVelocity.len();
    }

    @Override
    public void removeAllRibbons() {
        this.ribbons.clear();
    }

    @Override
    public void addRibbons(ArrayList<Ribbon> ribbons,float speedMult) {
        // do something with the speedMult because it should apply before the ribbons are locked in place
        this.ribbons.addAll(ribbons);
        locked = new boolean[ribbons.size()];
        this.initFormation();
    }

    @Override
    public void updateAllRibbons(float dt) {

        // all of the rotation and scaling happens in here... a LOT happens in here
        updateFormation(dt);

        //System.out.println("centre speed: " + this.formationCentreVelocity.len());

        // set the target position of every ribbon, and then move them towards it, or just move it directly if locked already
        for(int i = 0; i < ribbons.size(); i++) {

            // ribbons list is corresponding to flightFormationTargetPositionsAfterTranslation list
            Ribbon ribbon = ribbons.get(i);
            Vector3 ribbonTargetPosition = this.flightFormationTargetPositionsAfterTranslation.get(i);

            Vector3 oldPosition = ribbon.position.cpy();
            Vector3 towardsTarget = ribbonTargetPosition.cpy().sub(ribbon.position);

            // TODO: there is still a bug with ribbons not catching up to their spot so they can get locked. need to fix.
            // TODO: the symptoms are that SOME ribbons are not in the right position.
            // TODO: if you set all locked[i] true, here the symptoms don't show.

            // TODO: ALSO, THERE IS NO VELOCITY SET WHEN THEY ARE LOCKED, SO SOME RENDERERS DON'T LOOK RIGHT. EG, SOLAR, ROCKET.


            // debug 2019/07/15... TODO: locking is required very much, otherwise, for eg on the circular formation, the circle looks like it's tilted flat. the ribbons can't catch up.
            // TODO: need a fix for this soon. locking is needed but the ribbons have to catch up naturally before locking.
            //locked[i] = true;

            if(locked[i] == true) {
                ribbon.position.x = ribbonTargetPosition.x;
                ribbon.position.y = ribbonTargetPosition.y;
                ribbon.position.z = ribbonTargetPosition.z;
                // probably have to have some ribbon.rotationaxis calculation around here for the streamersrenderer
            }
            else {
                // steer towards target
                Vector3 towardsTargetVelocity = towardsTarget.cpy().nor().scl(approachSpeed * speedSettingFloat);
                if(towardsTargetVelocity.len() * dt > towardsTarget.len()) {
                    locked[i] = true;
                }
                else {
                    ribbon.velocity.add(towardsTargetVelocity);
                    ribbon.velocity.nor().scl(approachSpeed * speedSettingFloat);
                    ribbon.updatePosition(dt);
                }
            }

            // for color calculation
            float distanceTravelled = oldPosition.cpy().dst(ribbon.position);
            float ribbonSpeed = distanceTravelled / dt;

            ribbon.speed = ribbonSpeed;
            ribbon.velocity.nor().scl(ribbonSpeed);

            approachSpeed *= 1.000001f;
        }
    }

    // called just for random pitching
    private void pitch(float dt) {
        for(int i = 0; i < flightFormationRotatedButNotTranslated.size(); i++) {
            flightFormationRotatedButNotTranslated.get(i).rotate(this.pitchAxis,this.pitchRate * dt);
        }
        yawAxis.rotate(this.pitchAxis,this.pitchRate * dt);
        rollAxis.rotate(this.pitchAxis,this.pitchRate * dt);

        // testing
     //   pitchAxis.rotate(this.pitchAxis,this.pitchRate * dt);

        formationCentreVelocity.rotate(this.pitchAxis,this.pitchRate * dt);
    }

    // called in the process of steering the entire formation. this is typically preceded by a roll.
    private void pitch(float dt, float pitchRate) {
        for(int i = 0; i < flightFormationRotatedButNotTranslated.size(); i++) {
            flightFormationRotatedButNotTranslated.get(i).rotate(this.pitchAxis, pitchRate * dt);
        }
        yawAxis.rotate(this.pitchAxis, pitchRate * dt);
        rollAxis.rotate(this.pitchAxis, pitchRate * dt);

        // testing
      //  rollAxis.rotate(this.pitchAxis,pitchRate * dt);

        formationCentreVelocity.rotate(this.pitchAxis, pitchRate * dt);
    }

    // called just for random yawing
    private void yaw(float dt) {
        for(int i = 0; i < flightFormationRotatedButNotTranslated.size(); i++) {
            flightFormationRotatedButNotTranslated.get(i).rotate(this.yawAxis,this.yawRate * dt);
        }
        pitchAxis.rotate(this.yawAxis,this.yawRate * dt);
        rollAxis.rotate(this.yawAxis,this.yawRate * dt);

        // testing
//        yawAxis.rotate(this.yawAxis,this.yawRate * dt);

//        formationCentreVelocity.rotate(this.pitchAxis,this.pitchRate * dt);
        formationCentreVelocity.rotate(this.yawAxis,this.yawRate * dt);
    }

    // called just for random rolling
    private void roll(float dt) {
        for(int i = 0; i < flightFormationRotatedButNotTranslated.size(); i++) {
            flightFormationRotatedButNotTranslated.get(i).rotate(this.rollAxis,this.rollRate * dt);
        }
        yawAxis.rotate(this.rollAxis,this.rollRate * dt);
        pitchAxis.rotate(this.rollAxis,this.rollRate * dt);

        // testing
  //      rollAxis.rotate(this.rollAxis,this.rollRate * dt);

//        formationCentreVelocity.rotate(this.pitchAxis,this.pitchRate * dt);
        formationCentreVelocity.rotate(this.rollAxis,this.rollRate * dt);
    }

    // called in the process of steering the entire formation. this is typically followed by a pitch.
    private void roll(float dt, float rollRate) {
        for(int i = 0; i < flightFormationRotatedButNotTranslated.size(); i++) {
            flightFormationRotatedButNotTranslated.get(i).rotate(this.rollAxis, rollRate * dt);
        }
        yawAxis.rotate(this.rollAxis, rollRate * dt);
        pitchAxis.rotate(this.rollAxis, rollRate * dt);

        // testing
    //    rollAxis.rotate(this.rollAxis, rollRate * dt);

//        formationCentreVelocity.rotate(this.pitchAxis,this.pitchRate * dt);
        formationCentreVelocity.rotate(this.rollAxis, rollRate * dt);
    }


    private void updateFormation(float dt) {
        centrePosition.x += formationCentreVelocity.x * dt;
        centrePosition.y += formationCentreVelocity.y * dt;
        centrePosition.z += formationCentreVelocity.z * dt;

        // can reuse this for each mode
        float changeAcceleration;

        changeAcceleration = MIN_PITCH_RATE_CHANGE + rnd.nextFloat() * MAX_PITCH_RATE_CHANGE * 2;
        pitchAcceleration += changeAcceleration * dt;

        changeAcceleration = MIN_YAW_RATE_CHANGE + rnd.nextFloat() * MAX_YAW_RATE_CHANGE * 2;
        yawAcceleration += changeAcceleration * dt;

        changeAcceleration = MIN_ROLL_RATE_CHANGE + rnd.nextFloat() * MAX_ROLL_RATE_CHANGE * 2;
        rollAcceleration += changeAcceleration * dt;

//        pitchRate   += pitchAcceleration * dt;
  //      yawRate     += yawAcceleration * dt;
    //    rollRate    += rollAcceleration * dt;

        pitchRate   += pitchAcceleration ;
        yawRate     += yawAcceleration ;
        rollRate    += rollAcceleration ;


        if(pitchRate > MAX_PITCH_RATE) {
            pitchRate = MAX_PITCH_RATE;
        }
        else if(pitchRate < MIN_PITCH_RATE) {
            pitchRate = MIN_PITCH_RATE;
        }

        if(yawRate > MAX_YAW_RATE) {
            yawRate = MAX_YAW_RATE;
        }
        else if(yawRate < MIN_YAW_RATE) {
            yawRate = MIN_YAW_RATE;
        }

        if(rollRate > MAX_ROLL_RATE) {
            rollRate = MAX_ROLL_RATE;
        }
        else if(rollRate < MIN_ROLL_RATE) {
            rollRate = MIN_ROLL_RATE;
        }

        pitch(dt);
        yaw(dt);
        roll(dt);

        // copy the positions from flightFormationRotatedButNotTranslated to flightFormationTargetPositionsAfterTranslation, whilst adding the translate position to it.
        // this is because it is easier to store a copy of the flight formation at origin and rotate around origin
        // than it is to translate each ribbon to origin before rotating it.
        for(int i = 0; i < this.flightFormationRotatedButNotTranslated.size(); i++) {
            Vector3 position = this.flightFormationRotatedButNotTranslated.get(i).cpy().add(this.centrePosition);
            this.flightFormationTargetPositionsAfterTranslation.set(i, position);
        }

        // if last time we looked not all were locked, see if all are locked now
        if(allLocked == false) {
            boolean locked = true;
            for (int i = 0; i < this.locked.length; i++) {
                if(this.locked[i] == false) {
                    locked = false;
                    break;
                }
            }
            if(locked == true) {
                allLocked = true;
            }
        }

        if(
                (
                (centrePosition.x > BOUNDARY_RIGHT ) ||
                (centrePosition.x < BOUNDARY_LEFT  ) ||
                (centrePosition.y > BOUNDARY_BOTTOM) ||
                (centrePosition.y < BOUNDARY_TOP   ) ||
                (centrePosition.z > BOUNDARY_BACK  ) ||
                (centrePosition.z < BOUNDARY_FRONT )
                )
        ) {
            centreAccelerateHome(dt);
        }
        approachSpeedLimit();
    }


    private void centreAccelerateHome(float dt) {

        this.yawAcceleration = 0.0f;
        this.yawRate = 0.0f;

        this.rollAcceleration = 0.0f;
        this.rollRate = 0.0f;

//        this.rollAcceleration *= 0.9f;
  //      this.rollRate *= 0.9f;

        this.pitchAcceleration = 0.0f;
        this.pitchRate = 0.0f;

        // note - when these rates are too high, eg 180, it seems to get caught in a loop off screen. under 90 seems best.
        float degreesRoll   = 60.0f;
        float degreesPitch  = 60.0f;

        Vector3 pointingHomeNormalised = (new Vector3(0 - centrePosition.x, 0 - centrePosition.y, 500.0f - centrePosition.z)).nor();

        float leftDot           = getLeftDot(pointingHomeNormalised, -degreesRoll, degreesPitch);
        float rightDot          = getRightDot(pointingHomeNormalised, degreesRoll, degreesPitch);
        float straightDot       = getStraightDot(pointingHomeNormalised);
        float straightPullUpDot = getStraightPullDot(pointingHomeNormalised, degreesPitch);

        boolean leftBest            = leftDot > rightDot && leftDot > straightDot && leftDot > straightPullUpDot;
        boolean rightBest           = rightDot > leftDot && rightDot > straightDot && rightDot > straightPullUpDot;
        boolean straightBest        = straightDot > leftDot && straightDot > rightDot && straightDot > straightPullUpDot;
        boolean straightPullBest    = straightPullUpDot > leftDot && straightPullUpDot > rightDot && straightPullUpDot > straightDot;

        if(leftBest == true) {
            roll(dt, -degreesRoll);
            pitch(dt, degreesPitch);
        }
        else if(rightBest == true) {
            roll(dt, degreesRoll);
            pitch(dt, degreesPitch);
        }
        else if(straightBest == true) {

        }
        else if(straightPullBest == true) {
            pitch(dt, degreesPitch);
        }

    }

    private float getLeftDot(Vector3 pointingHomeNormalised, float degreesRoll, float degreesPitch) {
        // make copies because this function is just testing what would happen
        Vector3 rollAxis = this.rollAxis.cpy();
        Vector3 pitchAxis = this.pitchAxis.cpy();
        Vector3 turnLeftResult = formationCentreVelocity.cpy().nor();
        pitchAxis.rotate(rollAxis, degreesRoll);
        turnLeftResult.rotate(pitchAxis, degreesPitch);
        return pointingHomeNormalised.dot(turnLeftResult);
    }

    private float getRightDot(Vector3 pointingHomeNormalised, float degreesRoll, float degreesPitch) {
        // make copies because this function is just testing what would happen
        Vector3 rollAxis = this.rollAxis.cpy();
        Vector3 pitchAxis = this.pitchAxis.cpy();
        Vector3 turnRightResult = formationCentreVelocity.cpy().nor();
        pitchAxis.rotate(rollAxis, degreesRoll);
        turnRightResult.rotate(pitchAxis, degreesPitch);
        return pointingHomeNormalised.dot(turnRightResult);
    }

    private float getStraightDot(Vector3 pointingHomeNormalised) {
        Vector3 straightResult = formationCentreVelocity.cpy().nor();
        return pointingHomeNormalised.dot(straightResult);
    }

    private float getStraightPullDot(Vector3 pointingHomeNormalised, float degreesPitch) {
        // make copies because this function is just testing what would happen
        Vector3 pitchAxis = this.pitchAxis.cpy();
        Vector3 turnRightResult = formationCentreVelocity.cpy().nor();
        turnRightResult.rotate(pitchAxis, degreesPitch);
        return pointingHomeNormalised.dot(turnRightResult);
    }


    @Override
    public ArrayList<Ribbon> getRibbons() {
        return ribbons;
    }

    @Override
    public int getNextReplacedIndex() {
        lastReplacedIndex ++;
        if(lastReplacedIndex >= ribbons.size()) {
            lastReplacedIndex = 0;
        }
        return lastReplacedIndex;
    }

    @Override
    public int getMinimumFormations() {
        return MINIMUM_FORMATIONS;
    }

    @Override
    public int getMaximumFormations() {
        return MAXIMUM_FORMATIONS;
    }

    public void approachSpeedLimit() {
        if(formationCentreVelocity.len() > leaderSpeed) {
            formationCentreVelocity.scl(0.99f);
        }
        else if(formationCentreVelocity.len() < leaderSpeed) {
            formationCentreVelocity.scl(1.0f / 0.99f);
        }
    }

}
