package com.beachcafesoftware.ribbons;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by Guy Aaltonen on 23/08/17.
 */
public class Color {

    private static Random rnd = new Random();

    // constants are (start, magnitude-of-range)
    private static float[] RANGE_RED_GREEN = new float[] { 0f, 0.33f };
    private static float[] RANGE_YELLOW_CYAN = new float[] { 0f + 0.33f / 2f, 0.33f };
    private static float[] RANGE_GREEN_BLUE = new float[] { 0.33f, 0.33f };
    private static float[] RANGE_CYAN_MAGENTA = new float[] { 0.33f + 0.33f / 2f, 0.33f };
    private static float[] RANGE_BLUE_RED = new float[] { 0.66f, 0.33f };
    private static float[] RANGE_MAGENTA_YELLOW = new float[] { 0.66f + 0.33f / 2f, 0.33f };
    private static float[] RANGE_ALL = new float[] { 0f, 1.0f };

    private static String[] SCHEMES_TYPE_1 = new String[] { "redgreen","yellowcyan","greenblue","cyanmagenta","bluered", "magentayellow", "all" };
    private static String[] SCHEMES_TYPE_2 = new String[] { "time","timetripod","timebipod" };
	
    public int r;
    public int g ;
    public int b ;


    public Color(int r,int g,int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public Color(int r, int g, int b, int a) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    private static float[] colorSchemeToArray(String scheme) {
        if(scheme.equals("random") == true) {
            scheme = SCHEMES_TYPE_1[rnd.nextInt(SCHEMES_TYPE_1.length)];
        }

        if(scheme.equals("redgreen") == true) { return RANGE_RED_GREEN; }
        if(scheme.equals("yellowcyan") == true) { return RANGE_YELLOW_CYAN; }
        if(scheme.equals("greenblue") == true) { return RANGE_GREEN_BLUE; }
        if(scheme.equals("cyanmagenta") == true) { return RANGE_CYAN_MAGENTA; }
        if(scheme.equals("bluered") == true) { return RANGE_BLUE_RED; }
        if(scheme.equals("magentayellow") == true) { return RANGE_MAGENTA_YELLOW; }
        if(scheme.equals("all") == true) { return RANGE_ALL; }

        return null;
    }

    public static float[] getRandomColorFromScheme(String colorParams) {

//		float saturation = 0.75f;
        float saturation = 0.99f;
		float lightness = 0.075f * 0.0025f;

		if(arrayContainsString(SCHEMES_TYPE_1, colorParams)) {
			float[] colorParameters = colorSchemeToArray(colorParams);
			float hue = colorParameters[0] + rnd.nextFloat() * colorParameters[1];
			if(hue > 1.0f) { hue -= 1.0f; }
			return hslToRgb(hue, saturation, lightness);
		}
		
		if(arrayContainsString(SCHEMES_TYPE_2, colorParams)) {
			if(colorParams.equals("time") == true) {
				float hue = getBase();
                hue += rnd.nextFloat() * 0.33f;
				if(hue > 1.0f) { hue -= 1.0f; }
				return hslToRgb(hue, saturation, lightness);
			}
			if(colorParams.equals("timetripod") == true) {
				float hue = getBase();
                hue += rnd.nextFloat() * 0.05f;
				int random = rnd.nextInt(3);
				if(random == 1) { hue += 0.33f; }
				if(random == 2) { hue += 0.66f; }
				if(hue > 1.0f) { hue -= 1.0f; }
				return hslToRgb(hue, saturation, lightness);
			}
			if(colorParams.equals("timebipod") == true) {
				float hue = getBase();
                hue += rnd.nextFloat() * 0.05f;
				int random = rnd.nextInt(2);
				if(random == 1) { hue += 0.5f; }
				if(hue > 1.0f) { hue -= 1.0f; }
				return hslToRgb(hue, saturation, lightness);
			}
			
		}
        return null;
    }

    private static boolean arrayContainsString(String[] strings, String string) {
        for(int i = 0; i < strings.length; i++) {
            if(strings[i].equals(string) == true) {
                return true;
            }
        }
        return false;
    }

    private static float getBase() {
		// 100 buckets per day
		int SECONDS_PER_BUCKET = (60 * 60 * 24) / 100;
        Calendar c = Calendar.getInstance();
        int utcOffset = c.get(Calendar.ZONE_OFFSET) + c.get(Calendar.DST_OFFSET);
        Long utcMilliseconds = c.getTimeInMillis() + utcOffset;
        float base = ((utcMilliseconds / (1000 * SECONDS_PER_BUCKET)) % 100) / 100f;
        return base;
    }
	
    private static float[] hslToRgb(float h, float s, float l) {
        float r;
        float g;
        float b;

        if (s == 0) {
            r = g = b = l; // achromatic
        } else {
            float q = l < 0.5f ? l * (1 + s) : l + s - l * s;
            float p = 2 * l - q;
            r = hue2rgb(p, q, h + 1f / 3f);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1f / 3f);
        }
        return new float[] { r , g , b };
    }


    private static float hue2rgb(float p, float q, float t) {
        if (t < 0f) t += 1f;
        if (t > 1f) t -= 1f;
        if (t < 1f / 6f) return p + (q - p) * 6f * t;
        if (t < 1f / 2f) return q;
        if (t < 2f / 3f) return p + (q - p) * (2f / 3f - t) * 6f;
        return p;
    }

}
