#ifdef GL_ES
precision highp float;
#endif

//attributes from vertex shader
varying vec4 vColor;
varying vec3 vNormal;

//our texture samplers
uniform sampler2D u_texture0;   //diffuse map
uniform sampler2D u_normals;   //normal map

uniform vec3 LightPos;     //light position, normalized
varying vec3 fragPosition;

uniform sampler2D texture;
varying vec2 texCoord;

vec3 hueShift(vec3 color, float hueAdjust);


void main() {

    vec4 finalColor = vColor;

    // we stored the hue shift amount in the alpha channel, so set it to 1.0 after grabbing it
    float shiftAmount = -1.0 + finalColor.a;
    finalColor.a = 1.0;
        
    vec4 textureColor = texture2D(texture, texCoord);    
    float brightness = /*1.0 -*/ textureColor.r;
    vec4 combinedColor = finalColor * brightness * 2.0;
    vec3 hueShifted = hueShift(combinedColor.rgb, shiftAmount);
    gl_FragColor = vec4(hueShifted, 1.0);

    
    //gl_FragColor = texture2D(texture, texCoord) * finalColor * amplitude;

}



vec3 hueShift(vec3 color, float hueAdjust ){

    const vec3  kRGBToYPrime = vec3 (0.299, 0.587, 0.114);
    const vec3  kRGBToI      = vec3 (0.596, -0.275, -0.321);
    const vec3  kRGBToQ      = vec3 (0.212, -0.523, 0.311);

    const vec3  kYIQToR     = vec3 (1.0, 0.956, 0.621);
    const vec3  kYIQToG     = vec3 (1.0, -0.272, -0.647);
    const vec3  kYIQToB     = vec3 (1.0, -1.107, 1.704);

    float   YPrime  = dot (color, kRGBToYPrime);
    float   I       = dot (color, kRGBToI);
    float   Q       = dot (color, kRGBToQ);
    float   hue     = atan (Q, I);
    float   chroma  = sqrt (I * I + Q * Q);

    hue += hueAdjust;

    Q = chroma * sin (hue);
    I = chroma * cos (hue);

    vec3    yIQ   = vec3 (YPrime, I, Q);

    return vec3( dot (yIQ, kYIQToR), dot (yIQ, kYIQToG), dot (yIQ, kYIQToB) );

}

